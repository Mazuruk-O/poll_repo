package com.edu.donnu.auth_service.config;

import com.edu.donnu.auth_service.domain.Permission;
import com.edu.donnu.auth_service.domain.Role;
import com.edu.donnu.auth_service.domain.User;
import com.edu.donnu.auth_service.service.security.JpaUserDetailsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableAuthorizationServer
public class Oauth2Config extends AuthorizationServerConfigurerAdapter {

    private TokenStore tokenStore = new InMemoryTokenStore();

    private final String BCRYPT_PASSWORD_ENCODE = "{bcrypt}";

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    private JpaUserDetailsService jpaUserDetailsService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

        // TODO persist clients details

        // @formatter:off
        clients.inMemory()
                .withClient("browser")
                .authorizedGrantTypes("refresh_token", "password")
                .scopes("ui")
                .and()

                .withClient("account-service")
                .secret("$2a$10$4iCF3108kbFeaN1nYbeBcu5WA7LLc.VL/c41yNRfvvEwk7P4OHfWO")
                .authorizedGrantTypes("client_credentials", "refresh_token")
                .scopes("server")
                .and()

                .withClient("poll-service")
                .secret("$2a$10$4iCF3108kbFeaN1nYbeBcu5WA7LLc.VL/c41yNRfvvEwk7P4OHfWO")
                .authorizedGrantTypes("client_credentials", "refresh_token")
                .scopes("server")
                .and()

                .withClient("couting-poll-service")
                .secret("$2a$10$4iCF3108kbFeaN1nYbeBcu5WA7LLc.VL/c41yNRfvvEwk7P4OHfWO")
                .authorizedGrantTypes("client_credentials", "refresh_token")
                .scopes("server");
        // @formatter:on
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(tokenStore)
                .authenticationManager(authenticationManager)
                .userDetailsService(jpaUserDetailsService);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .passwordEncoder(bCryptPasswordEncoder);
    }


}
