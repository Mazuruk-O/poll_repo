package com.edu.donnu.auth_service.service;

import com.edu.donnu.auth_service.domain.User;

public interface UserService {

    void create(User user);

}
