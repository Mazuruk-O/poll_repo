package com.edu.donnu.auth_service.service;

import com.edu.donnu.auth_service.domain.User;
import com.edu.donnu.auth_service.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public void create(User user) {
        Optional<User> existing = userRepository.findByUsername(user.getUsername());
        if(existing.isPresent()){
            log.warn(("User already exists: " + existing.get().getUsername()));
            throw new IllegalArgumentException("User already exists: " + existing.get().getUsername());
        }

        String hash = encoder.encode(user.getPassword());
        user.setPassword(hash);

        userRepository.save(user);

        log.info("New user has been created: " + user.getUsername());

        return;
    }
}
