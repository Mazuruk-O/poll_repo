package com.edu.donnu.auth_service.controller;

import com.edu.donnu.auth_service.domain.AuthUserDetail;
import com.edu.donnu.auth_service.domain.Permission;
import com.edu.donnu.auth_service.domain.Role;
import com.edu.donnu.auth_service.domain.User;
import com.edu.donnu.auth_service.repository.PermissionRepository;
import com.edu.donnu.auth_service.repository.RoleRepository;
import com.edu.donnu.auth_service.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.security.auth.UserPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ControllerTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    @InjectMocks
    private Controller accountController;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Mock
    private UserService userService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(accountController).build();
    }

    @Test
    public void shouldCreateNewUser() throws Exception {
        Permission p = new Permission();
        p.setName("read");
        permissionRepository.save(p);

        Role r = new Role();
        r.setName("USER");
        r.setPermissions(Arrays.asList(p));
        roleRepository.save(r);

        User u = new User();
        u.setUsername("user");
        u.setPassword("password");
        u.setCredentialsNonExpired(true);
        u.setEnabled(true);
        u.setAccountNonExpired(true);
        u.setAccountNonLocked(true);
        u.setRoles(Arrays.asList(r));

        String json = mapper.writeValueAsString(u);

        mockMvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnCurrentUser() throws Exception {
        mockMvc.perform(get("/users/current").principal(new UserPrincipal("test")))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("test")));
    }

}