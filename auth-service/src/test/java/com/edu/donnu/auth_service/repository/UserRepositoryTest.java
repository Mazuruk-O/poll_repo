package com.edu.donnu.auth_service.repository;

import com.edu.donnu.auth_service.domain.Permission;
import com.edu.donnu.auth_service.domain.Role;
import com.edu.donnu.auth_service.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Test
    public void findByUsernameTest(){
        Permission p = new Permission();
        p.setName("read");
        permissionRepository.save(p);

        Role r = new Role();
        r.setName("USER");
        r.setPermissions(Arrays.asList(p));
        roleRepository.save(r);

        User u = new User();
        u.setUsername("user");
        u.setPassword("password");
        u.setCredentialsNonExpired(true);
        u.setEnabled(true);
        u.setAccountNonExpired(true);
        u.setAccountNonLocked(true);
        u.setRoles(Arrays.asList(r));
        repository.save(u);

        Optional<User> found = repository.findByUsername(u.getUsername());
        assertTrue(found.isPresent());
        assertEquals(u.getPassword(), found.get().getPassword());
        assertEquals(u.getUsername(), found.get().getUsername());
    }

}
