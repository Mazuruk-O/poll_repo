package ua.edu.donnu.couting_poll_service.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.edu.donnu.couting_poll_service.domain.alg_Gayla_Shepley.AlgGaylaShepley;

@Configuration
public class AppConfig {

    @Value("${spring.rabbitmq.host}")
    private String rabbitmqHost;

    @Value("${spring.rabbitmq.queue.toСalculate}")
    private String queueNameToCalculate;

    @Value("${spring.rabbitmq.queue.result}")
    private String queueNameResult;

    @Bean
    public AlgGaylaShepley algGaylaShepley(){
        return new AlgGaylaShepley();
    }

    /**
     * create Queue in rabbitmq with name "toСalculate"
     * @return Queue
     */
    @Bean
    public Queue myQueue1() {
        return new Queue(queueNameToCalculate);
    }

    /**
     * create Queue in rabbitmq with name "result"
     * @return Queue
     */
    @Bean
    public Queue myQueue12() {
        return new Queue(queueNameResult);
    }

    /**
     * for convert obj, who came from consumer
     * @return Jackson2JsonMessageConverter
     */
    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * for connect with rabbitmq
     * @return CachingConnectionFactory
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(rabbitmqHost);
        return connectionFactory;
    }

    /**
     * for registry/delete Queue in rabbitmq
     * @return AmqpAdmin
     */
    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    /**
     * for sending message to rabbitmq
     * it's producer
     * @return RabbitTemplate
     */
    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

}
