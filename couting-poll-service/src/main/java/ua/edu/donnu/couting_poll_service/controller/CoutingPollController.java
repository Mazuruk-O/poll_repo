package ua.edu.donnu.couting_poll_service.controller;

import com.netflix.discovery.converters.Auto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.edu.donnu.couting_poll_service.domain.DTO.PollDtoInput;
import ua.edu.donnu.couting_poll_service.domain.Pair;
import ua.edu.donnu.couting_poll_service.domain.PairResult;
import ua.edu.donnu.couting_poll_service.domain.ProcessingStatus;
import ua.edu.donnu.couting_poll_service.domain.alg_Gayla_Shepley.AlgGaylaShepley;
import ua.edu.donnu.couting_poll_service.service.PairResultService;
import ua.edu.donnu.couting_poll_service.service.PairService;
//import javax.ws.rs.Produces;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * <h1>Сontroller to get polls for counting and sending results</h1>
 *
 *  <p> Through the @PostMapping("/couting_poll") - we accept the results
 *  of the generated polls and submit the algorithm for processing. </p>
 *  <p> Through the @GetMapping("/couting_poll/{id}") - we send the results of the algorithm </p>
 *
 * @author  Mazuruk Oleg
 * @version 1.0
 * @since   2020-01-30
 */
@RestController
@Slf4j
public class CoutingPollController {

    @Autowired
    private AlgGaylaShepley algGaylaShepley;

    @Autowired
    private AmqpTemplate template;

    @Value("${spring.rabbitmq.queue.toСalculate}")
    private String queueNameToCalculate;

    @Autowired
    private PairResultService pairResultService;

    @Autowired
    private PairService pairService;

    /**
     * @param id - id poll for return obj
     * @return ResponseEntity with HttpStatus=404 and exeption or HttpStatus=200 and result poll
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getCoutingPoll(@PathVariable Long id){
        log.info("@GetMapping(\"/{id}\"): " + id);

        PairResult ps = pairResultService.findByIdPoll(id);

        List<Pair> pairs = pairService.findByPairResult(ps);

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(pairs);
    }

    /**
     * @param pollDtoInput - Data Transfer Object for poll
     * @return RsponseEntity with HttpStatus=201 or HttpStatus=400
     */
    @PostMapping("/")
    public ResponseEntity coutingPoll(@RequestBody PollDtoInput pollDtoInput){
        log.info("@GetMapping(\"/couting_poll\")");

        template.convertAndSend(queueNameToCalculate, pollDtoInput);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(pollDtoInput.getPoll());
    }

}
