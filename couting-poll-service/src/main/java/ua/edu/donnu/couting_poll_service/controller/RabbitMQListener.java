package ua.edu.donnu.couting_poll_service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.edu.donnu.couting_poll_service.domain.DTO.PollDtoInput;
import ua.edu.donnu.couting_poll_service.domain.DTO.PollDtoOutput;
import ua.edu.donnu.couting_poll_service.domain.PairResult;
import ua.edu.donnu.couting_poll_service.domain.ProcessingStatus;
import ua.edu.donnu.couting_poll_service.domain.alg_Gayla_Shepley.AlgGaylaShepley;
import ua.edu.donnu.couting_poll_service.service.PairResultService;

@Slf4j
@EnableRabbit
@Component
public class RabbitMQListener {

    @Value("${spring.rabbitmq.queue.toСalculate}")
    private String queueNameToCalculate;

    @Value("${spring.rabbitmq.queue.result}")
    private String queueNameResult;

    @Autowired
    private AlgGaylaShepley algGaylaShepley;

    @Autowired
    private AmqpTemplate template;

    @Autowired
    private PairResultService pairResultService;

    @RabbitListener(queues = "toСalculate")
    public void processQueue1(PollDtoInput pollDtoInput) {
        log.info("Received from queue 'toСalculate'. Poll id = " + pollDtoInput.getPoll().getIdPoll());

        ProcessingStatus ps = algGaylaShepley.calc(pollDtoInput);

        if(ps.equals(ProcessingStatus.SUCCESSFULLY)){

            PairResult pairResult = pairResultService.findByIdPoll(pollDtoInput.getPoll().getIdPoll());

            PollDtoOutput pollDtoOutput = new PollDtoOutput(pairResult);

            template.convertAndSend(queueNameResult, pollDtoOutput);
        }

        return;
    }

}
