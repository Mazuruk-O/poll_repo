package ua.edu.donnu.couting_poll_service.domain.DTO;

import lombok.Data;
import ua.edu.donnu.couting_poll_service.domain.Poll;
import java.util.List;
import java.util.Map;

@Data
public class PollDtoInput {
    // poll
    private Poll poll;
    // list teacher id
    private List<Long> teachers;
    //list student id
    private List<Long> students;
    // <student_id, List<teacher_id>>, priority 0 - max
    private Map<Long,List<Long>> studentsVoting;
    // <teacher, List<student_id>>, priority 0 - max
    private Map<Long,List<Long>> teacherVoting;

    private List<TeacherAndCountStudent> teacherAndCountStudents;
}
