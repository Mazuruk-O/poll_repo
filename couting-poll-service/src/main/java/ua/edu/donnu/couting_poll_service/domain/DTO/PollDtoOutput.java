package ua.edu.donnu.couting_poll_service.domain.DTO;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import ua.edu.donnu.couting_poll_service.domain.Pair;
import ua.edu.donnu.couting_poll_service.domain.PairResult;
import ua.edu.donnu.couting_poll_service.domain.Poll;
import ua.edu.donnu.couting_poll_service.service.PairService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class PollDtoOutput {

    @Autowired
    private PairService pairService;

    private Long idPoll;
    private String namePoll;
    private Map<Long,Long> pairResult;

    public PollDtoOutput(PairResult pr) {
        idPoll = pr.getIdPoll();
        namePoll = pr.getNamePoll();
        pairResult = createPairResult(pr);
    }

    private Map<Long, Long> createPairResult(PairResult pr) {
        List<Pair> pairs = pairService.findByPairResult(pr);
        Map<Long, Long> result = new HashMap<>();

        pairs.forEach(p -> result.put(p.getIdTeacher(), p.getIdStudent()));

        return result;
    }
}
