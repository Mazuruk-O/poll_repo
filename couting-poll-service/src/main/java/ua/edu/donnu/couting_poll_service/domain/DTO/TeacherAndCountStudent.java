package ua.edu.donnu.couting_poll_service.domain.DTO;

import lombok.Data;

@Data
public class TeacherAndCountStudent {
    private Long idTeacher;
    private Long countStudent;
}
