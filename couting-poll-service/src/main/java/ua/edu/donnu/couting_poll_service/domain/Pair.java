package ua.edu.donnu.couting_poll_service.domain;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "pairs")
public class Pair {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_pair")
    private Long idPair;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_pair_result")
    private PairResult pairResult;

    @NotNull
    @Column(name = "id_teacher")
    private Long idTeacher;

    @NotNull
    @Column(name = "id_student")
    private Long idStudent;

}
