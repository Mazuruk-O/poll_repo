package ua.edu.donnu.couting_poll_service.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "pair_results")
public class PairResult {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_pair_result")
    private Long idPairResult;

    @NotNull
    @Column(name = "id_poll")
    private Long idPoll;

    @NotNull
    @Column(name = "name_poll")
    private String namePoll;

}
