package ua.edu.donnu.couting_poll_service.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.sql.Date;

@Data
public class Poll {

    private Long idPoll;

    private String namePoll;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone="Europe/Kiev")
    private Date dateEndPoll;

}
