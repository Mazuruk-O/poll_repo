
package ua.edu.donnu.couting_poll_service.domain.alg_Gayla_Shepley;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.edu.donnu.couting_poll_service.domain.DTO.PollDtoInput;
import ua.edu.donnu.couting_poll_service.domain.DTO.TeacherAndCountStudent;
import ua.edu.donnu.couting_poll_service.domain.Pair;
import ua.edu.donnu.couting_poll_service.domain.PairResult;
import ua.edu.donnu.couting_poll_service.domain.Poll;
import ua.edu.donnu.couting_poll_service.domain.ProcessingStatus;
import ua.edu.donnu.couting_poll_service.service.PairResultService;
import ua.edu.donnu.couting_poll_service.service.PairService;

import java.util.*;

import static ua.edu.donnu.couting_poll_service.util.AlgGaylaShepleyUtil.*;

/**
 * <h1>Algorithm Gayla-Shepley</h1>
 *
 *  <p> Class is designed to calculate the results of
 *  	the survey using the Gayla-Shepley algorithm. </p>
 *
 * @author  Mazuruk Oleg
 * @version 1.0
 * @since   2020-01-30
 */
@Slf4j
@Component
public class AlgGaylaShepley {

	@Autowired
	private PairResultService pairResultService;

	@Autowired
	private PairService pairService;

	/**
	 * @param pollDtoInput - Data Transfer Object for poll
	 * @return HttpStatus=201 or HttpStatus=400
	 */
	public ProcessingStatus calc(PollDtoInput pollDtoInput){

		List<TeacherAndCountStudent> tacs = pollDtoInput.getTeacherAndCountStudents();
		Map<Long, List<Long>> mockFreePlaceTacher = mockFreePlace(pollDtoInput.getTeachers(),tacs);

		Map<Long, List<Long>> prefTeacher = formPrefTeacher(mockFreePlaceTacher, pollDtoInput.getTeacherVoting());
		Map<Long, List<Long>> prefStudent = formPrefStudent(mockFreePlaceTacher, pollDtoInput.getStudentsVoting());

		Map<Long,Long> resultAlgo = galeShapleyAlgo(prefTeacher.size(), prefTeacher,prefStudent,
							new ArrayList<>(prefTeacher.keySet()), new ArrayList<>(prefStudent.keySet()));

		// poll information
		Poll poll = pollDtoInput.getPoll();
		PairResult pr = new PairResult();
		pr.setIdPoll(poll.getIdPoll());
		pr.setNamePoll(poll.getNamePoll());
		pairResultService.save(pr);

		Map<Long, List<Long>> toSave = parseMockTeacher(mockFreePlaceTacher,resultAlgo);
		saveResult(toSave, pr);

		log.info("AlgGaylaShepley.class, method: calc() SUCCESSFULLY. idPoll = " + poll.getIdPoll());

		return ProcessingStatus.SUCCESSFULLY;
	}

	//algorithm implementation
	private Map<Long, Long> galeShapleyAlgo(int n, Map<Long, List<Long>> teacherPref,
			Map<Long, List<Long>> studentPref, List<Long> teachers, List<Long> students) {

		/// викладачі без студентів
		List<Long> teacherUnmatched = new ArrayList<>();

		// студенти без наукових керівників
		List<Long> studentUnmatched = new ArrayList<>();

		// матриця пар
		Map<Long, Long> pair = new HashMap<>();

		// Map для збереження пар які ми переглянули, починаючи від 0(нуль найвищий приорітет)
		Map<Long, Long> teacherIndex = new HashMap<>();
		
		for (int i = 0; i < n; i++) {
			teacherIndex.put(teachers.get(i), 0L);
		}
		
		for (int i = 0; i < n; i++) {
			teacherUnmatched.add(teachers.get(i));
			studentUnmatched.add(students.get(i));
		}

		// start time
		long startTime = System.nanoTime();

		// algorithm starts
		// поки всі пари не будуть сформовані
		while (!teacherUnmatched.isEmpty()) {

			/// отримуємо викладача який не має студента
			long man = teacherUnmatched.get(0);
			/// перелік приорітетів викладача
			List<Long> hisPref = teacherPref.get(man);
			/// приорітет студента з яким створюємо пару
			long indexnum = teacherIndex.get(man);
			/// знаходимо id студента
			long woman = hisPref.get((int)indexnum);
			/// отримуємо вектор приорітетів студента з яким буде створено пару
			List<Long> herPref = studentPref.get(woman);

			/// якщо даний студент не є з кимось в парі, ми створюємо пару
			if (studentUnmatched.contains(woman)) {
				/// створення пари
				pair.put(man, woman);
				/// видаляємо викладача і студента зі списку тих, хто немає пари
				teacherUnmatched.remove(teacherUnmatched.indexOf(man));
				studentUnmatched.remove(studentUnmatched.indexOf(woman));

				/// якщо дана жінка вже має пару і не була заручена з поточим man
			} else if (pair.containsValue(woman) && (hisPref.contains(woman))) {
				// отримуємо діючого наукового керівника
				long currentPartner = 0;
				for (Object partner : pair.keySet()) {
					if (pair.get(partner).equals(woman)) {
						currentPartner = (long) partner;
					}
				}
				/// якщо в приорітетах студента новий викладач вищий за поточного, ми формуємо нову пару
				if (herPref.indexOf(man) < herPref.indexOf(currentPartner)) {
					/// видаляєм стару пару
					pair.remove(currentPartner, woman);
					/// додаємо викладача в список без пар
					teacherUnmatched.add((long)currentPartner);
					/// створюємо нову пару
					pair.put(man, woman);
					/// видаляємо викладача, в якого "з"явилась пара"
					teacherUnmatched.remove(teacherUnmatched.indexOf(man));
				}
			}

			indexnum++;
			teacherIndex.put(man, indexnum);
		}
		
		// end time
		long endTime = System.nanoTime();
		long totalTime = (endTime - startTime) / 1000;

		log.info("N = " + n + " Time took to run algorithm " + totalTime + " ms");

		return pair;

	}

	private void saveResult(Map<Long, List<Long>> toSave, PairResult pr) {
		toSave.forEach((idTeacher, students) -> {
			students.forEach(s -> {
				Pair p = new Pair();
				p.setIdTeacher(idTeacher);
				p.setPairResult(pr);
				p.setIdStudent(s);
				pairService.save(p);
			});
		});

		return;
	}

	//method for test algorithm
	//this method generated randomly shuffled preference list
	public void preflist(int n) {
		List<Long> teachers = new ArrayList<>(n);
		List<Long> students = new ArrayList<>(n);

		for (long i = 0; i < n; i++) {
			teachers.add(i);
			students.add(n+i);
		}

		/// матриця вибору викладачів
		Map<Long, List<Long>> teacherPref = new HashMap<>();
		/// матриця вибору студентів
		Map<Long, List<Long>> studentPref = new HashMap<>();

		for (int i = 0; i < n; i++) {
			/// приорітет і-го студента
			List<Long> studentTemp = new ArrayList<>();
			/// приорітет і-го викладачв
			List<Long> teacherTemp = new ArrayList<>();

			for (int j = 0; j < n; j++) {
				studentTemp.add(students.get(j));
				teacherTemp.add(teachers.get(j));
			}

			/// перемішуємо приорітети
			Collections.shuffle(studentTemp);
			Collections.shuffle(teacherTemp);

			/// додаємо в матрицю виборів - вибір конкретного викладача(menlist.get(i))
			/// та його приорітети(womantemp)
			teacherPref.put(teachers.get(i), studentTemp);

			/// аналогічно для студентів
			studentPref.put(students.get(i), teacherTemp);

		}

		galeShapleyAlgo(n,teacherPref,studentPref,teachers,students);

	}
}
