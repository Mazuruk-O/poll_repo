package ua.edu.donnu.couting_poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.edu.donnu.couting_poll_service.domain.PairResult;

import java.util.Optional;

public interface PairResultRepository extends JpaRepository<PairResult, Long> {
    Optional<PairResult> findByNamePoll(String namePoll);
    Optional<PairResult> findByIdPoll(Long idPoll);
}
