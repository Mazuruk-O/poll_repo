package ua.edu.donnu.couting_poll_service.service;

import java.util.List;

public interface DataService<T> {

    T save(T entity);

    T findById(Long id);

    List<T> findAll();

    void delete(T entity);

}
