package ua.edu.donnu.couting_poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.couting_poll_service.domain.Pair;
import ua.edu.donnu.couting_poll_service.domain.PairResult;
import ua.edu.donnu.couting_poll_service.repository.PairResultRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PairResultService implements DataService<PairResult> {

    @Autowired
    private PairResultRepository pairResultRepository;

    @Autowired
    private PairService pairService;

    @Override
    public PairResult save(PairResult entity) {
        pairResultRepository.save(entity);

        log.info("Save PairResult with id: " + entity.getIdPairResult());

        return entity;
    }

    @Override
    public PairResult findById(Long id) {
        Optional<PairResult> optional = pairResultRepository.findById(id);

        if(optional.isPresent()){
            log.info("Find PairResult with id: " + optional.get().getIdPairResult());

            return optional.get();
        }

        log.warn("Not find PairResult with id: " + id);

        return new PairResult();
    }

    @Override
    public List<PairResult> findAll() {
        List<PairResult> pairs = pairResultRepository.findAll();

        if(!pairs.isEmpty()){
            log.info("Find all PairResult");

            return pairs;
        }

        log.warn("Not find all PairResult");

        return new ArrayList<>();
    }

    public PairResult findByNamePoll(String namePoll){
        if(Objects.nonNull(namePoll)){
            Optional<PairResult> optional = pairResultRepository.findByNamePoll(namePoll);

            if(optional.isPresent()){
                log.info("Find PairResult by Poll name: " + namePoll);

                return optional.get();
            }
        }

        log.warn("Not find PairResult by Poll name: " + namePoll);

        return new PairResult();
    }

    public PairResult findByIdPoll(Long idPoll){
        if(Objects.nonNull(idPoll)){
            Optional<PairResult> optional = pairResultRepository.findByIdPoll(idPoll);

            if(optional.isPresent()){
                log.info("Find PairResult by Poll id: " + idPoll);

                return optional.get();
            }
        }

        log.warn("Not find PairResult by Poll id: " + idPoll);

        return new PairResult();
    }

    @Override
    public void delete(PairResult entity) {
        List<Pair> pairs = pairService.findByPairResult(entity);

        pairs.forEach(p -> pairService.delete(p));

        log.warn("Delete PairResult with id: " + entity.getIdPoll());

        pairResultRepository.delete(entity);

        return;
    }
}
