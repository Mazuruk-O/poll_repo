package ua.edu.donnu.couting_poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.couting_poll_service.domain.Pair;
import ua.edu.donnu.couting_poll_service.domain.PairResult;
import ua.edu.donnu.couting_poll_service.repository.PairRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PairService implements DataService<Pair> {

    @Autowired
    private PairRepository pairRepository;

    @Override
    public Pair save(Pair entity) {
        pairRepository.save(entity);

        log.info("Save Pair with id: " + entity.getIdPair());

        return entity;
    }

    @Override
    public Pair findById(Long id) {
        Optional<Pair> optionalFaculty = pairRepository.findById(id);

        if(optionalFaculty.isPresent()){
            log.info("Find Pair with id: " + id);
            return optionalFaculty.get();
        }

        log.warn("Not find Pair with id: " + id);

        return new Pair();
    }

    public List<Pair> findByPairResult(PairResult pairResult){
        if(Objects.nonNull(pairResult) && Objects.nonNull(pairResult.getIdPairResult())){
            List<Pair> pairs = pairRepository.findByPairResult(pairResult);

            if(!pairs.isEmpty()){
                log.info("Find Pair with by PairResult with id: " + pairResult.getIdPairResult());

                return pairs;
            }
        }

        log.warn("Not find Pair by PairResult with id: " + pairResult.getIdPairResult());

        return new ArrayList<>();
    }

    @Override
    public List<Pair> findAll() {
        List<Pair> pairs = pairRepository.findAll();

        if(!pairs.isEmpty()){
            log.info("Find all Pair");

            return pairs;
        }

        log.warn("Not find all Pair");

        return new ArrayList<>();
    }

    @Override
    public void delete(Pair entity) {
        log.warn("Delete Pair with id: " + entity.getIdPair());

        pairRepository.delete(entity);

        return;
    }
}
