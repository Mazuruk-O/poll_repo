package ua.edu.donnu.couting_poll_service.util;

import ua.edu.donnu.couting_poll_service.domain.DTO.TeacherAndCountStudent;

import java.util.*;

public class AlgGaylaShepleyUtil {

    public static void testForStableMatch(Map<Integer, Integer> result, Map<Integer, List<Integer>> menpref,
                                   Map<Integer, List<Integer>> womanpref) {

        for (Map.Entry<Integer, Integer> matched : result.entrySet()) {

            Integer man = matched.getKey();

            Integer woman = matched.getValue();

            List<Integer> hisPref = menpref.get(man);

            for (int i = 0; i < hisPref.size(); i++) {

                int tempwoman = hisPref.get(i);

                if (tempwoman == woman) {
                    break;
                } else {
                    // getting current partner
                    int tempWomanPartner = 0;

                    for (Object partner : result.keySet()) {
                        if (result.get(partner).equals(tempwoman)) {
                            tempWomanPartner = (int) partner;
                        }

                    }

                    List<Integer> herPref = womanpref.get(tempwoman);

                    if (herPref.indexOf(man) < herPref.indexOf(tempWomanPartner)) {
                        System.out.println(man + " and " + tempwoman
                                + " are unstable partner and prefer each other over their current partner");

                    }
                }
            }

            System.out.println(man + " and " + woman + " are stable match");

        }
    }

    public static Map<Long, List<Long>> formPrefStudent(Map<Long, List<Long>> mockFreePlaceTacher, Map<Long, List<Long>> studentsVoting) {
        mockFreePlaceTacher.forEach((idTeacher,mocksIdTeacher) -> {
            studentsVoting.forEach((idStudent,prefStudent) -> {
                int indexTeach = prefStudent.indexOf(idTeacher);
                prefStudent.addAll(indexTeach,mocksIdTeacher);
                prefStudent.remove(idTeacher);
            });
        });

        return studentsVoting;
    }

    public static Map<Long, List<Long>> formPrefTeacher(Map<Long, List<Long>> mockFreePlaceTacher, Map<Long, List<Long>> teacherVoting) {
        Map<Long, List<Long>> result = new HashMap<>();

        mockFreePlaceTacher.forEach((k,v) -> {
            v.forEach(current -> {
                List<Long> prefCurrent = teacherVoting.get(k);
                result.put(current,prefCurrent);
            });
        });

        return result;
    }

    public static Map<Long, List<Long>> mockFreePlace(List<Long> teachers, List<TeacherAndCountStudent> tacs) {
        Random random = new Random();

        Map<Long, List<Long>> result = new HashMap<>();
        while (!tacs.isEmpty()){
            List<Long> cloneNumId = new ArrayList<>();
            TeacherAndCountStudent current = tacs.get(0);
            Long countStudent = current.getCountStudent();

            while (countStudent != 0){
                Long mock = random.nextLong();
                if(!teachers.contains(mock)){
                    cloneNumId.add(mock);
                    countStudent--;
                }
            }

            result.put(current.getIdTeacher(),cloneNumId);
            tacs.remove(current);
        }

        return result;
    }

    public static Map<Long, List<Long>> parseMockTeacher(Map<Long, List<Long>> mockFreePlaceTacher, Map<Long, Long> resultAlgo) {
        Map<Long,List<Long>> toSave = new HashMap<>();

        mockFreePlaceTacher.forEach((idTeacher, mocksId) -> {
            List<Long> students = new ArrayList<>();
            mocksId.forEach((current -> {
                students.add(resultAlgo.get(current));
            }));
            toSave.put(idTeacher,students);
        });

        return toSave;
    }
}
