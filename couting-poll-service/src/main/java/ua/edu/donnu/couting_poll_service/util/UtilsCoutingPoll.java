package ua.edu.donnu.couting_poll_service.util;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <h1>Auxiliary class for calculations</h1>
 *
 * @author  Mazuruk Oleg
 * @version 1.0
 * @since   2020-01-30
 */
public class UtilsCoutingPoll {

    /**
     * @param personsChoise - a list of people who passed the survey
     * @param <T> - Teacher.class or Student.class
     * @return List<Long> - id all person
     */
    public static <T> List<Long> getAllPersonId(List<T> personsChoise){

        /*List<Long> list = new ArrayList<>(personsChoise.size());

        for(T t : personsChoise){
            list.add(getIdPerson(t));
        }*/

        return null;
    }

    /**
     *
     * @param t - the person we get the ID from
     * @param <T> - Teacher.class or Student.class
     * @return Long - id person
     * @throws IllegalArgumentException - if <T> is not Teacher.class or Student.class
     */
    private static <T> Long getIdPerson(T t) throws IllegalArgumentException {

        /*if(t instanceof StudentChoise){

            StudentChoise sc = (StudentChoise)t;

            return sc.getIdVotingStudent();

        } else if(t instanceof TeacherChoise){

            TeacherChoise tc = (TeacherChoise)t;

            return tc.getIdVotingTeacher();

        } else {

            throw new IllegalArgumentException("Incorrect data: " + t + "\t UtilsCoutingPoll.class");

        }*/

        return null;
    }

    /**
     *
     * @param personsChoise - a list of people who passed the survey
     * @param <T> - Teacher.class or Student.class
     * @return Map<Long,List<Long>> - key: id person; value: priority list
     */
    public static <T> Map<Long,List<Long>> getPersonsPriority(List<T> personsChoise){

        /*Map<Long,List<Long>> priority = new HashMap<>(personsChoise.size(), 1.0f);

        for(T t : personsChoise){
            Long idPerson = getIdPerson(t);
            List<Long> priorityPerson = getListPriority(t);
            priority.put(idPerson, priorityPerson);
        }

        return priority;*/

        return null;

    }

    /**
     * @param t - the person we get the priority list from
     * @param <T> - Teacher.class or Student.class
     * @return List<Long> - priority list person
     * @throws IllegalArgumentException - if <T> is not Teacher.class or Student.class
     */
    private static <T> List<Long> getListPriority(T t) throws IllegalArgumentException {

        /*if(t instanceof StudentChoise){

            StudentChoise sc = (StudentChoise)t;

            Map<Long,Long> pr = sc.getChoisePairStudent().entrySet().stream()
                                  .sorted(Map.Entry.comparingByKey())
                                  .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                  (oldValue, newValue) -> oldValue, LinkedHashMap::new));

            return new ArrayList<>(pr.values());

        } else if(t instanceof TeacherChoise){

            TeacherChoise sc = (TeacherChoise)t;

            Map<Long,Long> pr = sc.getChoisePairTeacher().entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));

            return new ArrayList<>(pr.values());

        } else {
            throw new IllegalArgumentException("Incorrect data: " + t + "\t UtilsCoutingPoll.class");
        }*/

        return null;
    }
}
