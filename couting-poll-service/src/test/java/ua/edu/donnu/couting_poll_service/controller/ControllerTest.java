package ua.edu.donnu.couting_poll_service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.edu.donnu.couting_poll_service.domain.Pair;
import ua.edu.donnu.couting_poll_service.domain.PairResult;
import ua.edu.donnu.couting_poll_service.service.PairResultService;
import ua.edu.donnu.couting_poll_service.service.PairService;

import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ControllerTest {

    @InjectMocks
    protected CoutingPollController coutingPollController;

    @Mock
    protected PairService pairService;

    @Mock
    protected PairResultService pairResultService;

    private static final ObjectMapper mapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setup() {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(coutingPollController).build();
    }

    @Test
    public void findByIdTest() throws Exception {
        PairResult ps = new PairResult();
        ps.setNamePoll("name");
        ps.setIdPoll(1L);
        pairResultService.save(ps);

        Pair p = new Pair();
        p.setIdPair(1L);
        p.setPairResult(ps);
        p.setIdStudent(1L);
        p.setIdTeacher(1L);
        pairService.save(p);

        when(pairResultService.findByIdPoll(1L)).thenReturn(ps);
        when(pairService.findByPairResult(ps)).thenReturn(Arrays.asList(p));

        String json = mapper.writeValueAsString(Arrays.asList(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/" + 1L))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(json)));
    }

}
