package ua.edu.donnu.couting_poll_service.domain.alg_Gayla_Shepley;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.edu.donnu.couting_poll_service.domain.DTO.PollDtoInput;
import ua.edu.donnu.couting_poll_service.domain.DTO.TeacherAndCountStudent;
import ua.edu.donnu.couting_poll_service.domain.Poll;
import ua.edu.donnu.couting_poll_service.domain.ProcessingStatus;

import java.sql.Date;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AlgGaylaShepleyTest {

    @Autowired
    private AlgGaylaShepley algGaylaShepley;

    @Test
    public void calcDiffValue() {
        algGaylaShepley.preflist(5);
        algGaylaShepley.preflist(10);
        algGaylaShepley.preflist(20);
        algGaylaShepley.preflist(50);
        algGaylaShepley.preflist(100);
    }

    @Test
    public void calc2(){
        PollDtoInput dto = new PollDtoInput();
        Poll poll = new Poll();
        poll.setNamePoll("name");
        poll.setIdPoll(1L);
        poll.setDateEndPoll(new Date(System.currentTimeMillis()));
        dto.setPoll(poll);

        List<Long> teachers = Arrays.asList(1L,2L,3L);
        List<Long> students = Arrays.asList(10L,11L,12L,13L,14L);

        dto.setTeachers(teachers);
        dto.setStudents(students);

        Map<Long,List<Long>> teacherVoting = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            List<Long> tmp = new ArrayList<>(students);
            Collections.shuffle(tmp);
            teacherVoting.put(teachers.get(i), tmp);
        }

        Map<Long,List<Long>> studentVoting = new HashMap<>();
        for (int i = 0; i < 5; i++) {
            List<Long> tmp = new ArrayList<>(teachers);
            Collections.shuffle(tmp);
            studentVoting.put(students.get(i), tmp);
        }

        dto.setTeacherVoting(teacherVoting);
        dto.setStudentsVoting(studentVoting);

        System.out.println(" ===== Priority Teacher ===== ");
        teacherVoting.forEach((k,v) -> System.out.println("idTeacher: " + k + ", prefTeacher: " + Arrays.toString(v.toArray())));
        System.out.println("\n ===== Priority Student ===== ");
        studentVoting.forEach((k,v) -> System.out.println("idStudent: " + k + ", prefTeacher: " + Arrays.toString(v.toArray())));

        List<TeacherAndCountStudent> tacs = new LinkedList<>();
        TeacherAndCountStudent t1 = new TeacherAndCountStudent();
        t1.setIdTeacher(1L);
        t1.setCountStudent(1L);
        TeacherAndCountStudent t2 = new TeacherAndCountStudent();
        t2.setIdTeacher(2L);
        t2.setCountStudent(2L);
        TeacherAndCountStudent t3 = new TeacherAndCountStudent();
        t3.setIdTeacher(3L);
        t3.setCountStudent(2L);

        tacs.add(t1);
        tacs.add(t2);
        tacs.add(t3);

        dto.setTeacherAndCountStudents(tacs);

        System.out.println("\n ===== Teacher and count Student ===== ");
        tacs.forEach((v) -> System.out.println("idTeacher: " + v.getIdTeacher() + ", countStudent: " + v.getCountStudent()));

        ProcessingStatus ps = algGaylaShepley.calc(dto);
        assertEquals(ProcessingStatus.SUCCESSFULLY, ps);
    }

    @Test
    public void calc3(){
        PollDtoInput dto = new PollDtoInput();
        Poll poll = new Poll();
        poll.setNamePoll("name");
        poll.setIdPoll(1L);
        poll.setDateEndPoll(new Date(System.currentTimeMillis()));
        dto.setPoll(poll);

        List<Long> teachers = Arrays.asList(1L,2L,3L);
        List<Long> students = Arrays.asList(10L,11L,12L,13L,14L);

        dto.setTeachers(teachers);
        dto.setStudents(students);

        Map<Long,List<Long>> teacherVoting = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            List<Long> tmp = new ArrayList<>(students);
            teacherVoting.put(teachers.get(i), tmp);
        }

        Map<Long,List<Long>> studentVoting = new HashMap<>();
        for (int i = 0; i < 5; i++) {
            List<Long> tmp = new ArrayList<>(teachers);
            studentVoting.put(students.get(i), tmp);
        }

        dto.setTeacherVoting(teacherVoting);
        dto.setStudentsVoting(studentVoting);

        System.out.println(" ===== Priority Teacher ===== ");
        teacherVoting.forEach((k,v) -> System.out.println("idTeacher: " + k + ", prefTeacher: " + Arrays.toString(v.toArray())));
        System.out.println("\n ===== Priority Student ===== ");
        studentVoting.forEach((k,v) -> System.out.println("idStudent: " + k + ", prefTeacher: " + Arrays.toString(v.toArray())));

        List<TeacherAndCountStudent> tacs = new LinkedList<>();
        TeacherAndCountStudent t1 = new TeacherAndCountStudent();
        t1.setIdTeacher(1L);
        t1.setCountStudent(1L);
        TeacherAndCountStudent t2 = new TeacherAndCountStudent();
        t2.setIdTeacher(2L);
        t2.setCountStudent(2L);
        TeacherAndCountStudent t3 = new TeacherAndCountStudent();
        t3.setIdTeacher(3L);
        t3.setCountStudent(2L);

        tacs.add(t1);
        tacs.add(t2);
        tacs.add(t3);

        dto.setTeacherAndCountStudents(tacs);

        System.out.println("\n ===== Teacher and count Student ===== ");
        tacs.forEach((v) -> System.out.println("idTeacher: " + v.getIdTeacher() + ", countStudent: " + v.getCountStudent()));

        ProcessingStatus ps = algGaylaShepley.calc(dto);
        assertEquals(ProcessingStatus.SUCCESSFULLY, ps);
    }
}