package ua.edu.donnu.couting_poll_service.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.edu.donnu.couting_poll_service.domain.Pair;
import ua.edu.donnu.couting_poll_service.domain.PairResult;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AllRepositoryTest {

    @Autowired
    private PairRepository pairRepository;

    @Autowired
    private PairResultRepository pairResultRepository;

    @Test
    public void pairResultTest(){
        PairResult ps = new PairResult();
        ps.setNamePoll("name");
        ps.setIdPoll(11L);
        pairResultRepository.save(ps);
        assertNotNull(ps.getIdPairResult());

        PairResult ps1 = pairResultRepository.findByIdPoll(11L).get();
        PairResult ps2 = pairResultRepository.findByNamePoll("name").get();

        assertEquals(ps,ps1);
        assertEquals(ps,ps2);
    }

    @Test
    public void pairTest(){
        PairResult ps = new PairResult();
        ps.setNamePoll("name");
        ps.setIdPoll(11L);
        pairResultRepository.save(ps);
        assertNotNull(ps.getIdPairResult());

        Pair p = new Pair();
        p.setPairResult(ps);
        p.setIdStudent(1L);
        p.setIdTeacher(1L);
        pairRepository.save(p);

        List<Pair> p1 = pairRepository.findByPairResult(ps);
        assertTrue(p1.contains(p));
    }

}
