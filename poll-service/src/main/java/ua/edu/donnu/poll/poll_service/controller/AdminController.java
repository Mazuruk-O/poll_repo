package ua.edu.donnu.poll.poll_service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.edu.donnu.poll.poll_service.domain.DTO.input.InputDtoCreateFaculty;
import ua.edu.donnu.poll.poll_service.domain.DTO.input.InputDtoCreateGroup;
import ua.edu.donnu.poll.poll_service.domain.Faculty;
import ua.edu.donnu.poll.poll_service.domain.Group;
import ua.edu.donnu.poll.poll_service.service.FacultyService;
import ua.edu.donnu.poll.poll_service.service.GroupService;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private FacultyService facultyService;

    @PostMapping("/group")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> createGroup(@RequestBody InputDtoCreateGroup dto){
        log.info("@PostMapping(\"admin/group\")");

        List<Group> groups = dto.getGroups();

        groups.forEach((g) -> groupService.save(g));

        groups.forEach((g) -> log.info("Save Groups with id: " + g.getIdGroup()));

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    @PostMapping("/faculty")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> createFaculty(@RequestBody InputDtoCreateFaculty dto){
        System.out.println("@PostMapping(\"admin/faculty\")");

        List<Faculty> faculties = dto.getFacultys();

        faculties.forEach((f) -> facultyService.save(f));

        faculties.forEach((f) -> log.info("Save Faculty with id: " + f.getIdFaculty()));

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

}
