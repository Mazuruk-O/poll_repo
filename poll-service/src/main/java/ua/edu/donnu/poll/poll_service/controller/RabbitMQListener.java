package ua.edu.donnu.poll.poll_service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.edu.donnu.poll.poll_service.domain.DTO.input.InputDtoResultCoutingPoll;
import ua.edu.donnu.poll.poll_service.domain.PairResult;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.Student;
import ua.edu.donnu.poll.poll_service.domain.Teacher;
import ua.edu.donnu.poll.poll_service.service.PairResultService;
import ua.edu.donnu.poll.poll_service.service.PollService;
import ua.edu.donnu.poll.poll_service.service.StudentService;
import ua.edu.donnu.poll.poll_service.service.TeacherService;
import java.util.Map;

@Slf4j
@EnableRabbit
@Component
public class RabbitMQListener {

    @Autowired
    private PollService pollService;

    @Autowired
    private PairResultService pairResultService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @RabbitListener(queues = "result")
    public void processQueue1(InputDtoResultCoutingPoll dto) {
        log.info("@RabbitListener(queues = \"result\"). Poll with id: " + dto.getIdPoll());

        Poll poll = pollService.findById(dto.getIdPoll());

        safePairResult(poll, dto.getPairResult());

        return;
    }

    private void safePairResult(Poll poll, Map<Long, Long> pairs) {
        pairs.forEach((k,v) -> {
            PairResult pairResult = new PairResult();
            pairResult.setPoll(poll);
            pairResult.setTeacher(findTeacherById(k));
            pairResult.setStudent(findStudentById(v));

            pairResultService.save(pairResult);
        });

        log.info("Save PairResult for Poll id: " + poll.getIdPoll());

        return;
    }

    private Student findStudentById(Long v) {
        return studentService.findById(v);
    }

    private Teacher findTeacherById(Long k) {
        return teacherService.findById(k);
    }

}
