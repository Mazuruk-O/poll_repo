package ua.edu.donnu.poll.poll_service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.domain.DTO.input.InputDtoStudentChoise;
import ua.edu.donnu.poll.poll_service.domain.DTO.output.OutputDtoAllPolls;
import ua.edu.donnu.poll.poll_service.domain.DTO.output.OutputDtoCurrentPollStudent;
import ua.edu.donnu.poll.poll_service.service.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private PollService pollService;

    @Autowired
    private StudentChoiseService studentChoiseService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private PairResultService pairResultService;

    @Autowired
    private PriorityStudentService priorityStudentService;

    @Autowired
    private TeacherAndCountStudentService teacherAndCountStudentService;

    /**
     * @return List<Poll> polls - all poll for current student
     */
    @GetMapping("/all-poll")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> getAllPoll(Authentication authentication){
        System.out.println(authentication);
        Student st = studentService.findByUsername(authentication.getName());
        List<Poll> polls = pollService.findByGroup(st.getGroup());

        log.info("@GetMapping(\"/poll/student\"); studentName: " + authentication.getName());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new OutputDtoAllPolls(polls));
    }

    /**
     *
     * @return OutputDtoCurrentPollStudent
     */
    @GetMapping("/poll/{id}")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> getCurrentPoll(@PathVariable Long id, Authentication authentication){
        Student st = studentService.findByUsername(authentication.getName());
        Poll poll = pollService.findById(id);
        OutputDtoCurrentPollStudent dto = new OutputDtoCurrentPollStudent();

        if(st.getGroup().equals(poll.getGroup())){
            log.info("@GetMapping(\"/poll/{id}\"). Find Poll with id: " + id + ", and name: " + poll.getNamePoll());
            StudentChoise stch = studentChoiseService.findByVotingStudentAndPoll(st,poll);
            List<TeacherAndCountStudent> tacs = teacherAndCountStudentService.findByPoll(poll);

            dto.setPoll(poll);
            dto.setTeacherAndCountStudents(tacs);
            dto.setPriorityStudents(priorityStudentService.findByStudentChoise(stch));
            dto.setPairResult(pairResultService.findByStudentAndPoll(st, poll));

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(dto);
        }

        log.warn("@GetMapping(\"/poll/{id}\"). Access denied to Poll with id: " + id + ", and name: " + poll.getNamePoll() +
                    "Student username: " + st.getUsername());

        dto.setPoll(new Poll());
        dto.setTeacherAndCountStudents(new ArrayList<>());
        dto.setPairResult(new PairResult());
        dto.setPriorityStudents(new ArrayList<>());

        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    /**
     *
     * @param dto
     * @param authentication
     * @return
     */
    @PostMapping("/poll")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> createStudentChoise(@RequestBody InputDtoStudentChoise dto, Authentication authentication){
        Student st = studentService.findByUsername(authentication.getName());
        Poll poll = pollService.findById(dto.getPoll().getIdPoll());
        List<PriorityStudent> priorityStudents = dto.getPriorityStudent();

        if(st.getGroup().equals(poll.getGroup())){
            StudentChoise stch = new StudentChoise();
            stch.setPoll(poll);
            stch.setVotingStudent(st);
            studentChoiseService.save(stch);

            priorityStudents.forEach((ps) ->{ ps.setStudentChoise(stch);
                                              priorityStudentService.save(ps);});

            log.info("@PostMapping(\\\"/choise\\\"), created StudentChoise with id: " + stch.getIdStudentChoise() +
                        " for Poll: " + poll.getNamePoll());

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(priorityStudents);
        }

        log.warn("@PostMapping(\\\"/choise\\\"), Access denied to create StudentChoise for Student with username: "+
                    st.getUsername() + " and Poll id: " + poll.getIdPoll());

        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .contentType(MediaType.APPLICATION_JSON)
                .body(priorityStudents);
    }

    @PutMapping("/poll")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_STUDENT')")
    public ResponseEntity<?> refreshStudentChoise(@RequestBody InputDtoStudentChoise dto, Authentication authentication){
        Student st = studentService.findByUsername(authentication.getName());
        Poll poll = pollService.findById(dto.getPoll().getIdPoll());
        List<PriorityStudent> priorityStudents = dto.getPriorityStudent();
        List<PriorityStudent> priorityStudentsRefresh = null;
        StudentChoise stch = null;

        if(st.getGroup().equals(poll.getGroup())){
            stch = studentChoiseService.findByVotingStudentAndPoll(st,poll);

            priorityStudentsRefresh = priorityStudentService.findByStudentChoise(stch);

            priorityStudentsRefresh.sort(PriorityStudent.byId);
            priorityStudents.sort(PriorityStudent.byId);

            for(PriorityStudent refresh : priorityStudentsRefresh){
                for(PriorityStudent ps : priorityStudents){
                    if (refresh.getIdPriorityStudent().equals(ps.getIdPriorityStudent())) {
                        refresh.setTeacher(ps.getTeacher());
                        refresh.setPriority(ps.getPriority());
                        priorityStudentService.save(refresh);
                        break;
                    }
                }
            }
        }

        log.info("@PutMapping(\"/choise\"), refresh StudentChoise with id: " + stch.getIdStudentChoise());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(priorityStudentsRefresh);
    }
}
