package ua.edu.donnu.poll.poll_service.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.domain.DTO.input.InputDtoCreatePoll;
import ua.edu.donnu.poll.poll_service.domain.DTO.input.InputDtoTeacherChoise;
import ua.edu.donnu.poll.poll_service.domain.DTO.output.OutputDtoAllPolls;
import ua.edu.donnu.poll.poll_service.domain.DTO.output.OutputDtoCurrentPollTeacher;
import ua.edu.donnu.poll.poll_service.service.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Autowired
    private PollService pollService;

    @Autowired
    private TeacherChoiseService teacherChoiseService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private PriorityTeacherService priorityTeacherService;

    @Autowired
    private TeacherAndCountStudentService tacss;

    @Autowired
    private StudentService studentService;

    @Autowired
    private PairResultService pairResultService;

    /**
     * @return List<Poll> polls
     */
    @GetMapping("/")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getAllPoll(Authentication authentication){
        Teacher tch = teacherService.findByUsername(authentication.getName());
        List<Poll> polls = new ArrayList<>();
        tacss.findByTeacher(tch).forEach((t) -> polls.add(t.getPoll()));

        log.info("@GetMapping(\"/poll/teacher\"); teacherName: " + authentication.getName());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new OutputDtoAllPolls(polls));
    }


    /**
     * ToDO
     * @return Poll
     */
    @GetMapping("/{id}")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> getCurrentPoll(@PathVariable Long id, Authentication authentication){
        Teacher tch = teacherService.findByUsername(authentication.getName());
        Poll poll = pollService.findById(id);
        OutputDtoCurrentPollTeacher dto = new OutputDtoCurrentPollTeacher();

        boolean isEnabled = false;

        for (TeacherAndCountStudent t: tacss.findByPoll(poll)) {
            if(t.getTeacher().equals(tch)){
                isEnabled = true;
                break;
            }
        }

        if(isEnabled){
            TeacherChoise teacherChoise = teacherChoiseService.findByPollAndVotingTeacher(poll,tch);
            dto.setPoll(poll);
            dto.setStudents(studentService.findByGroup(poll.getGroup()));
            dto.setPriorityTeacher(priorityTeacherService.findByTeacherChoise(teacherChoise));
            dto.setPairResult(pairResultService.findByPoll(poll));

            log.info("@GetMapping(\"/poll/\"). Find Poll with id: " + id + ", and name: " + poll.getNamePoll());

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(dto);
        }

        dto.setPoll(new Poll());
        dto.setStudents(new ArrayList<>());
        dto.setPairResult(new ArrayList<>());
        dto.setPriorityTeacher(new ArrayList<>());

        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    @PostMapping("/")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> createTeacherChoise(@RequestBody InputDtoTeacherChoise dto, Authentication authentication){
        Teacher tch = teacherService.findByUsername(authentication.getName());
        Poll poll = pollService.findById(dto.getPoll().getIdPoll());
        List<PriorityTeacher> priorityTeacher = dto.getPriorityTeacher();

        TeacherChoise teacherChoise = new TeacherChoise();
        teacherChoise.setVotingTeacher(tch);
        teacherChoise.setPoll(poll);

        priorityTeacher.forEach((pt) -> {
            pt.setTeacherChoise(teacherChoise);
            pt.setStudent(studentService.findByUsername(pt.getStudent().getUsername()));
            priorityTeacherService.save(pt);
        });

        log.info("");

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    /**
     * ToDo
     */
    @PostMapping("/poll")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> createPoll(@RequestBody InputDtoCreatePoll dto, Authentication authentication){
        Teacher tch = teacherService.findByUsername(authentication.getName());
        Poll poll = dto.getPoll();
        poll.setTeacherCreator(tch);
        pollService.save(poll);

        List<TeacherAndCountStudent> result = saveTeacherAndCountStudent(dto.getTeacherAndCountStudents(),poll);
        dto.setTeacherAndCountStudents(result);

        log.info("@PostMapping(\"/poll\")");

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    /**
     *
     * @param dto
     * @param authentication
     * @return
     */
    @PutMapping("/")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> updateTeacherChoise(@RequestBody InputDtoTeacherChoise dto, Authentication authentication){
        Teacher tch = teacherService.findByUsername(authentication.getName());
        Poll poll = pollService.findById(dto.getPoll().getIdPoll());
        List<PriorityTeacher> priorityTeacher = dto.getPriorityTeacher();
        List<PriorityTeacher> result = new ArrayList<>();

        priorityTeacher.forEach((pt) -> {
            PriorityTeacher refresh = priorityTeacherService.findById(pt.getIdPriorityTeacher());
            refresh.setPriority(pt.getPriority());
            priorityTeacherService.save(refresh);
            result.add(refresh);
        });

        dto.setPriorityTeacher(result);

        log.info("@PutMapping(\"/\")");

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    /**
     * ToDo
     */
    @PutMapping("/poll")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> refreshPoll(@RequestBody InputDtoCreatePoll dto, Authentication authentication){
        Poll poll = dto.getPoll();
        pollService.save(poll);

        List<TeacherAndCountStudent> result = saveTeacherAndCountStudent(dto.getTeacherAndCountStudents(),poll);
        dto.setTeacherAndCountStudents(result);

        log.info("@PutMapping(\"/poll\")");

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    /**
     * ToDo
     */
    @DeleteMapping("/poll/{id}")
    @PreAuthorize("#oauth2.hasScope('server') or hasRole('ROLE_TEACHER')")
    public ResponseEntity<?> deletePoll(@PathVariable Long id, Authentication authentication){
        Teacher tch = teacherService.findByUsername(authentication.getName());
        Poll poll = pollService.findById(id);

        if(poll.getTeacherCreator().equals(tch)){
            pollService.delete(poll);

            log.warn("@DeleteMapping(\"/poll/{id}\"): " + id);

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(poll);
        }

        log.warn("@DeleteMapping(\"/poll/{id}\"): " + id);

        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .contentType(MediaType.APPLICATION_JSON)
                .body(id);
    }

    private List<TeacherAndCountStudent> saveTeacherAndCountStudent(List<TeacherAndCountStudent> teacherAndCountStudents, Poll poll) {
        List<TeacherAndCountStudent> result = new ArrayList<>();

        teacherAndCountStudents.forEach((tacs) -> {
            Teacher search = tacs.getTeacher();
            Teacher t = teacherService.findByFirstnameAndLastnameAndSurname(search.getFirstnameTeacher(),
                    search.getLastnameTeacher(),
                    search.getSurnameTeacher()).get(0);
            TeacherAndCountStudent teacherAndCountStudent = new TeacherAndCountStudent();
            teacherAndCountStudent.setPoll(poll);
            teacherAndCountStudent.setTeacher(t);
            teacherAndCountStudent.setCountStudent(tacs.getCountStudent());
            result.add(teacherAndCountStudent);

        });

        return result;
    }
}
