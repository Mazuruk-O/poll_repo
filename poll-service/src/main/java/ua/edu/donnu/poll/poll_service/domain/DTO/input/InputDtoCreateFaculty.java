package ua.edu.donnu.poll.poll_service.domain.DTO.input;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.Faculty;
import java.util.List;

@Data
public class InputDtoCreateFaculty {
    private List<Faculty> facultys;
}
