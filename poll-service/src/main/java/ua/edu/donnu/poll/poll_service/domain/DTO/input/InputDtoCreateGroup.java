package ua.edu.donnu.poll.poll_service.domain.DTO.input;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.Group;
import java.util.List;

@Data
public class InputDtoCreateGroup {
    List<Group> groups;
}
