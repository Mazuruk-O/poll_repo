package ua.edu.donnu.poll.poll_service.domain.DTO.input;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.TeacherAndCountStudent;
import java.util.List;

@Data
public class InputDtoCreatePoll {
    private Poll poll;
    private List<TeacherAndCountStudent> teacherAndCountStudents;
}
