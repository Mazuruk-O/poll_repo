package ua.edu.donnu.poll.poll_service.domain.DTO.input;

import lombok.Data;
import java.util.Map;

@Data
public class InputDtoResultCoutingPoll {
    private Long idPoll;
    private String namePoll;
    private Map<Long,Long> pairResult;
}
