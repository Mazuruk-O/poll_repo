package ua.edu.donnu.poll.poll_service.domain.DTO.input;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.PriorityStudent;
import java.util.List;

@Data
public class InputDtoStudentChoise {
    private Poll poll;
    private List<PriorityStudent> priorityStudent;
}
