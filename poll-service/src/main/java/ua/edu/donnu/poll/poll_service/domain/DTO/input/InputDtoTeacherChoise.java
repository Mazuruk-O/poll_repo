package ua.edu.donnu.poll.poll_service.domain.DTO.input;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.PriorityTeacher;
import java.util.List;

@Data
public class InputDtoTeacherChoise {
    private Poll poll;
    private List<PriorityTeacher> priorityTeacher;
}
