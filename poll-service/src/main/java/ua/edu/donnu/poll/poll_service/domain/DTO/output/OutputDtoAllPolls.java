package ua.edu.donnu.poll.poll_service.domain.DTO.output;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.Poll;

import java.util.List;

@Data
public class OutputDtoAllPolls {

    private List<Poll> polls;

    public OutputDtoAllPolls(List<Poll> polls) {
        this.polls = polls;
    }
}
