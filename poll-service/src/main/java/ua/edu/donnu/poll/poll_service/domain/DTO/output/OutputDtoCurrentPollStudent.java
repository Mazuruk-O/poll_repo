package ua.edu.donnu.poll.poll_service.domain.DTO.output;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.PairResult;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.PriorityStudent;
import ua.edu.donnu.poll.poll_service.domain.TeacherAndCountStudent;

import java.util.List;

@Data
public class OutputDtoCurrentPollStudent {
    private Poll poll;
    private List<TeacherAndCountStudent> teacherAndCountStudents;
    private List<PriorityStudent> priorityStudents;
    private PairResult pairResult;

    public OutputDtoCurrentPollStudent() {
    }

    public OutputDtoCurrentPollStudent(Poll poll, List<TeacherAndCountStudent> teacherAndCountStudents, List<PriorityStudent> priorityStudents, PairResult pairResult) {
        this.poll = poll;
        this.teacherAndCountStudents = teacherAndCountStudents;
        this.priorityStudents = priorityStudents;
        this.pairResult = pairResult;
    }
}
