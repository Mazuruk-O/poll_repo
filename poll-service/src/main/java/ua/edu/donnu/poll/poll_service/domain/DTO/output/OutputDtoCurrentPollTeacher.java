package ua.edu.donnu.poll.poll_service.domain.DTO.output;

import lombok.Data;
import ua.edu.donnu.poll.poll_service.domain.PairResult;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.PriorityTeacher;
import ua.edu.donnu.poll.poll_service.domain.Student;
import java.util.List;

@Data
public class OutputDtoCurrentPollTeacher {

    private Poll poll;
    private List<Student> students;
    private List<PriorityTeacher> priorityTeacher;
    private List<PairResult> pairResult;

    public OutputDtoCurrentPollTeacher() {
    }

    public OutputDtoCurrentPollTeacher(Poll poll, List<Student> students, List<PriorityTeacher> priorityTeacher, List<PairResult> pairResult) {
        this.poll = poll;
        this.students = students;
        this.priorityTeacher = priorityTeacher;
        this.pairResult = pairResult;
    }
}
