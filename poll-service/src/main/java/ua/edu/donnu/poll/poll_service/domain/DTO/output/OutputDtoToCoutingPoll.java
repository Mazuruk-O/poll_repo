package ua.edu.donnu.poll.poll_service.domain.DTO.output;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.service.*;
import java.util.*;

@Data
public class OutputDtoToCoutingPoll {

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherAndCountStudentService teacherAndCountStudentService;

    @Autowired
    private StudentChoiseService studentChoiseService;

    @Autowired
    private TeacherChoiseService teacherChoiseService;

    @Autowired
    private PriorityStudentService priorityStudentService;

    @Autowired
    private PriorityTeacherService priorityTeacherService;

    // poll
    private Poll poll;
    // list teacher id
    private Map<Long, Long> teachersAndCountStudent;
    //list student id
    private List<Long> students;
    // <student_id, List<teacher_id>>, priority 0 - max
    private Map<Long,List<Long>> studentsVoting;
    // <teacher, List<student_id>>, priority 0 - max
    private Map<Long,List<Long>> teacherVoting;

    public OutputDtoToCoutingPoll(Poll poll) {
        this.poll = poll;

        students = selectStudentId(poll);

        teachersAndCountStudent = selectTeacherIdAndCountStudent(poll);

        studentsVoting = selectStudentVoting(poll);

        teacherVoting = selectTeacherVoting(poll);

        return;
    }

    private List<Long> selectStudentId(Poll poll) {
        List<Student> students = studentService.findByGroup(poll.getGroup());

        List<Long> studentId = new ArrayList<>();
        students.forEach(s -> studentId.add(s.getIdStudent()));

        return studentId;
    }

    private Map<Long, Long> selectTeacherIdAndCountStudent(Poll poll) {
        List<TeacherAndCountStudent> listTacs = teacherAndCountStudentService.findByPoll(poll);

        Map<Long, Long> teachersAndCountStudent = new HashMap<>();
        listTacs.forEach(tacs -> teachersAndCountStudent.put(tacs.getTeacher().getIdTeacher(), tacs.getCountStudent()));

        return teachersAndCountStudent;
    }

    private Map<Long, List<Long>> selectStudentVoting(Poll poll) {
        List<StudentChoise> sc = studentChoiseService.findByPoll(poll);

        Map<Long, List<Long>> studentVoting = new HashMap<>();

        sc.forEach(studentChoise -> {
            List<PriorityStudent> ps = priorityStudentService.findByStudentChoise(studentChoise);
            List<Long> priority = new ArrayList<>();

            ps.sort((o1, o2) -> (int) (o1.getPriority() - o2.getPriority()));

            ps.forEach(p -> priority.add(p.getTeacher().getIdTeacher()));

            studentsVoting.put(studentChoise.getVotingStudent().getIdStudent(), priority);
        });

        return studentVoting;
    }

    private Map<Long, List<Long>> selectTeacherVoting(Poll poll) {
        List<TeacherChoise> tc = teacherChoiseService.findByPoll(poll);

        Map<Long, List<Long>> teacherVoting = new HashMap<>();

        tc.forEach(teacherChoise -> {
            List<PriorityTeacher> pt = priorityTeacherService.findByTeacherChoise(teacherChoise);
            List<Long> priority = new ArrayList<>();

            pt.sort((o1, o2) -> (int) (o1.getPriority() - o2.getPriority()));

            pt.forEach(p -> priority.add(p.getStudent().getIdStudent()));

            teacherVoting.put(teacherChoise.getVotingTeacher().getIdTeacher(), priority);
        });

        return teacherVoting;
    }
}
