package ua.edu.donnu.poll.poll_service.domain;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Table(name = "faculties")
public class Faculty implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_faculty")
    private Long idFaculty;

    @NotNull
    @Column(name = "full_name_faculty", unique = true)
    private String fullNameFaculty;

    @NotNull
    @Column(name = "short_name_faculty")
    private String shortNameFaculty;

}
