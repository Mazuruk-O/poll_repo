package ua.edu.donnu.poll.poll_service.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "polls")
public class Poll implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_poll")
    private Long idPoll;

    @NotNull
    @Column(name = "name_poll")
    private String namePoll;

    @NotNull
    @Column(name = "date_end_poll")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone="Europe/Kiev")
    private Date dateEndPoll;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_group")
    private Group group;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_teacher")
    private Teacher teacherCreator;

}