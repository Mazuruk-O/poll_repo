package ua.edu.donnu.poll.poll_service.domain;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Comparator;

@Data
@Entity
@Table(name = "priority_student")
public class PriorityStudent implements Serializable {

    public static final Comparator<PriorityStudent> byId = Comparator.comparing(PriorityStudent::getIdPriorityStudent);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_student_priority")
    private Long idPriorityStudent;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_student_choise")
    private StudentChoise studentChoise;

    @NotNull
    @Column(name = "priority")
    private Long priority;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_teacher")
    private Teacher teacher;

}
