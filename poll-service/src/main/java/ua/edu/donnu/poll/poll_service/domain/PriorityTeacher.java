package ua.edu.donnu.poll.poll_service.domain;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Comparator;

@Data
@Entity
@Table(name = "teacher_priority")
public class PriorityTeacher implements Serializable {

    public static final Comparator<PriorityTeacher> byId = Comparator.comparing(PriorityTeacher::getIdPriorityTeacher);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_teacher_priority")
    private Long idPriorityTeacher;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_teacher_choise")
    private TeacherChoise teacherChoise;

    @NotNull
    @Column(name = "priority")
    private Long priority;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_student")
    private Student student;

}
