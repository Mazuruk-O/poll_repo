package ua.edu.donnu.poll.poll_service.domain;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Table(name = "students")
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_student")
    private Long idStudent;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "firstname_student")
    private String firstnameStudent;

    @NotNull
    @Column(name = "lastname_student")
    private String lastnameStudent;

    @NotNull
    @Column(name = "surname_student")
    private String surnameStudent;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_group")
    private Group group;

}