package ua.edu.donnu.poll.poll_service.domain;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "teachers")
public class Teacher implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_teacher")
    private Long idTeacher;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "firstname_teacher")
    private String firstnameTeacher;

    @NotNull
    @Column(name = "lastname_teacher")
    private String lastnameTeacher;

    @NotNull
    @Column(name = "surname_teacher")
    private String surnameTeacher;

}
