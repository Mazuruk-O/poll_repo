package ua.edu.donnu.poll.poll_service.domain.scheduler;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import ua.edu.donnu.poll.poll_service.domain.DTO.output.OutputDtoToCoutingPoll;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.service.PollService;
import java.util.Objects;

public class DelayedPollLaunch implements Runnable {

    @Autowired
    private AmqpTemplate template;

    @Autowired
    private PollService pollService;

    @Value("${spring.rabbitmq.queue.toСalculate}")
    private String queueNameToCalculate;

    private Long idPoll;

    public DelayedPollLaunch(Poll poll) {
        if(Objects.nonNull(poll.getIdPoll())){
            this.idPoll = poll.getIdPoll();
            return;
        }

        throw new IllegalArgumentException("Error value idPoll: " + poll.getIdPoll());
    }

    @Override
    public void run() {
        Poll poll = pollService.findById(idPoll);

        OutputDtoToCoutingPoll outputDtoToCoutingPoll = new OutputDtoToCoutingPoll(poll);

        template.convertAndSend(queueNameToCalculate, outputDtoToCoutingPoll);

        return;
    }
}
