package ua.edu.donnu.poll.poll_service.domain.security;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * insert into user (id, username,password, email, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked)
 * VALUES
 * (null, 'user','{bcrypt}$2a$10$4iCF3108kbFeaN1nYbeBcu5WA7LLc.VL/c41yNRfvvEwk7P4OHfWO', 'k@krishantha.com', '1', '1', '1', '1');
 */

@Data
public class User implements UserDetails, Serializable {

    private Long id;

    private String username;

    private String password;

    private boolean accountNonExpired;

    private boolean accountNonLocked;

    private boolean credentialsNonExpired;

    private boolean enabled;

    private List<GrantedAuthority> grantedAuthorities;

    public User(String username, String password, boolean accountNonExpired, boolean accountNonLocked,
                boolean credentialsNonExpired, boolean enabled, List<String> roles) {
        this.username = username;
        this.password = password;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
        setRole(roles);
        return;
    }

    public User() {
    }

    private void setRole(List<String> roles) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        roles.forEach(role -> grantedAuthorities.add(new SimpleGrantedAuthority(role)));

        this.grantedAuthorities = grantedAuthorities;

        return;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
