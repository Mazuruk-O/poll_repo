package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.Faculty;
import ua.edu.donnu.poll.poll_service.domain.Group;
import java.util.List;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findByFullNameGroup(String fullNameGroup);
    List<Group> findGroupsByFaculty(Faculty faculty);
}
