package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.PairResult;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.Student;
import ua.edu.donnu.poll.poll_service.domain.Teacher;
import java.util.List;
import java.util.Optional;

@Repository
public interface PairResultRepository extends JpaRepository<PairResult, Long> {
    List<PairResult> findByPoll(Poll poll);
    List<PairResult> findByTeacher(Teacher teacher);
    List<PairResult> findByStudent(Student student);
    Optional<PairResult> findByStudentAndPoll(Student student,Poll poll);
    Optional<PairResult> findByTeacherAndPoll(Teacher teacher,Poll poll);
}
