package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.PriorityStudent;
import ua.edu.donnu.poll.poll_service.domain.StudentChoise;
import java.util.List;

@Repository
public interface PriorityStudentRepository extends JpaRepository<PriorityStudent,Long> {
    List<PriorityStudent> findByStudentChoise(StudentChoise studentChoise);
}
