package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.PriorityTeacher;
import ua.edu.donnu.poll.poll_service.domain.TeacherChoise;
import java.util.List;

@Repository
public interface PriorityTeacherRepository extends JpaRepository<PriorityTeacher, Long> {
    List<PriorityTeacher> findByTeacherChoise(TeacherChoise teacherChoise);
}
