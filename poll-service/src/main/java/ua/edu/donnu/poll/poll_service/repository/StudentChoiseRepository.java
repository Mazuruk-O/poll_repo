package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.Student;
import ua.edu.donnu.poll.poll_service.domain.StudentChoise;
import java.util.List;
import java.util.Optional;

@Repository
public interface StudentChoiseRepository extends JpaRepository<StudentChoise, Long> {
    List<StudentChoise> findByPoll(Poll poll);
    List<StudentChoise> findByVotingStudent(Student votingStudent);
    Optional<StudentChoise> findByVotingStudentAndPoll(Student votingStudent,Poll poll);
}
