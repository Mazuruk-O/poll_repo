package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.Group;
import ua.edu.donnu.poll.poll_service.domain.Student;
import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository  extends JpaRepository<Student, Long> {
    List<Student> findByGroup(Group group);
    Optional<Student> findByUsername(String username);
    List<Student> findByFirstnameStudentAndLastnameStudentAndSurnameStudent(String firstnameStudent, String lastnameStudent, String surnameStudent);
}
