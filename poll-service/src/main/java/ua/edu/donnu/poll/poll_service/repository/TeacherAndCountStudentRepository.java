package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.Teacher;
import ua.edu.donnu.poll.poll_service.domain.TeacherAndCountStudent;
import java.util.List;
import java.util.Optional;

@Repository
public interface TeacherAndCountStudentRepository extends JpaRepository<TeacherAndCountStudent, Long> {
    List<TeacherAndCountStudent> findByPoll(Poll poll);
    List<TeacherAndCountStudent> findByTeacher(Teacher teacher);
    Optional<TeacherAndCountStudent> findByTeacherAndPoll(Teacher teacher, Poll poll);
}
