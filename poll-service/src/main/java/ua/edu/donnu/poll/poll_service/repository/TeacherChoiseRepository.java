package ua.edu.donnu.poll.poll_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.Teacher;
import ua.edu.donnu.poll.poll_service.domain.TeacherChoise;
import java.util.List;
import java.util.Optional;

@Repository
public interface TeacherChoiseRepository extends JpaRepository<TeacherChoise, Long> {
    List<TeacherChoise> findByPoll(Poll poll);
    List<TeacherChoise> findByVotingTeacher(Teacher votingTeacher);
    Optional<TeacherChoise> findByPollAndVotingTeacher(Poll poll, Teacher votingTeacher);
}
