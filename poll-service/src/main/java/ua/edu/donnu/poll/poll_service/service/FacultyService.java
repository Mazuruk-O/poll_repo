package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.ObjectDeletedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.Faculty;
import ua.edu.donnu.poll.poll_service.repository.FacultyRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class FacultyService implements DataService<Faculty> {

    @Autowired
    private FacultyRepository facultyRepository;

    /**
     * @param entity - object to save
     * @return if the save is successful: saved object
     *                           or else: empty object
     */
    @Override
    public Faculty save(Faculty entity) {
        Faculty saveEntity = facultyRepository.save(entity);

        log.info("Save Faculty with id:" + saveEntity.getIdFaculty());

        return saveEntity;
    }

    /**
     * @param id - id entity for search
     * @return if the search is successful: find object
     *                             or else: empty object
     */
    @Override
    public Faculty findById(Long id) {
        Optional<Faculty> optionalFaculty = facultyRepository.findById(id);

        if(optionalFaculty.isPresent()){
            log.info("Find faculty with id:" + optionalFaculty.get().getIdFaculty());

            return optionalFaculty.get();
        }

        log.warn("Not find Faculty with id:" + id);

        return new Faculty();
    }

    /**
     * @return if the search is successful: find List<Entity>
     *                             or else: empty List<Entity>
     */
    @Override
    public List<Faculty> findAll() {
        List<Faculty> facultys = facultyRepository.findAll();

        if(!facultys.isEmpty()){
            log.info("Find all Faculty");

            return facultys;
        }

        log.warn("Not find all Faculty");

        return new ArrayList<>();
    }

    /**
     * @param fullNameFaculty
     * @return
     */
    public Faculty findByFullNameFaculty(String fullNameFaculty){
        if(Objects.nonNull(fullNameFaculty) || !(fullNameFaculty.length() < 5)){
            Faculty faculty = facultyRepository.findByFullNameFaculty(fullNameFaculty);

            if(Objects.nonNull(faculty)){
                log.info("Find faculty with id: " + faculty.getIdFaculty());

                return faculty;
            }
        }

        log.warn("Not find Faculty with fullname: " + fullNameFaculty);

        return new Faculty();
    }

    /**
     * it is impossible to delete the faculty
     * @param entity - object to delete
     * @throws ObjectDeletedException - always
     */
    @Override
    public void delete(Faculty entity) throws ObjectDeletedException {

        throw new ObjectDeletedException(("Error delete Faculty: " + entity), entity, "Faculty.class");

    }
}
