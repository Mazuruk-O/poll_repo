package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.ObjectDeletedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.Faculty;
import ua.edu.donnu.poll.poll_service.domain.Group;
import ua.edu.donnu.poll.poll_service.repository.GroupRepository;
import java.util.*;

@Slf4j
@Service
public class GroupService implements DataService<Group> {

    @Autowired
    private GroupRepository groupRepository;

    /**
     * @param entity - object to save
     * @return if the save is successful: saved object
     *                           or else: empty object
     */
    public Group save(Group entity) {
        Group saveEntity = groupRepository.save(entity);

        log.info("Save group with id: " + saveEntity.getIdGroup());

        return saveEntity;
    }

    /**
     * @param id - id entity for search
     * @return if the search is successful: find object
     *                             or else: empty object
     */
    @Override
    public Group findById(Long id) {
        Optional<Group> optionalGroup = groupRepository.findById(id);

        if(optionalGroup.isPresent()){
            log.info("Find Group with id: " + optionalGroup.get().getIdGroup());

            return optionalGroup.get();
        }

        log.warn("Not find Group with id:" + id);

        return new Group();
    }

    /**
     * @return if the search is successful: find List<Entity>
     *                             or else: empty List<Entity>
     */
    @Override
    public List<Group> findAll() {
        List<Group> groups = groupRepository.findAll();

        if(!groups.isEmpty()){
            log.info("Find all Group");

            return groups;
        }

        log.warn("Not find all Group");

        return new ArrayList<>();
    }

    /**
     * ToDo: write docum
     * @param faculty
     * @return
     */
    public List<Group> findGroupsByFaculty(Faculty faculty){
        if(Objects.nonNull(faculty.getIdFaculty())){
            List<Group> groups = groupRepository.findGroupsByFaculty(faculty);

            if(!groups.isEmpty()){
                log.info("Find Group by Faculty with id: " + faculty.getIdFaculty());

                return groups;
            }
        }

        log.warn("Not find Group by Faculty with id: " + faculty.getIdFaculty());

        return new ArrayList<>();
    }

    /**
     * ToDo: write docum
     * @param fullNameGroup
     * @return
     */
    public Group findByFullNameGroup(String fullNameGroup){
        if(Objects.nonNull(fullNameGroup) || !(fullNameGroup.length() < 5)){
            Group group = groupRepository.findByFullNameGroup(fullNameGroup);

            if(Objects.nonNull(group)){
                log.info("Find Group with id:" + group.getIdGroup());

                return group;
            }
        }

        log.warn("Not find Group by fullNameGroup: " + fullNameGroup);

        return new Group();
    }

    /**
     * it is impossible to delete the group
     * @param entity - object to delete
     * @throws ObjectDeletedException - always
     */
    @Override
    public void delete(Group entity) throws ObjectDeletedException {

        throw new ObjectDeletedException(("Error delete Group: " + entity), entity, "Group.class");

    }
}
