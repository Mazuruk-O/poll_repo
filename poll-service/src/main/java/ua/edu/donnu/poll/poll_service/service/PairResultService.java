package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.repository.PairResultRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PairResultService implements DataService<PairResult>{

    @Autowired
    private PairResultRepository pairResultRepository;

    /**
     * ToDo
     * @param entity
     * @return
     */
    @Override
    public PairResult save(PairResult entity) {
        pairResultRepository.save(entity);

        log.info("Save PairResult with id: " + entity.getIdPairResult());

        return entity;
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public PairResult findById(Long id) {
        Optional<PairResult> optionalPairResult = pairResultRepository.findById(id);

        if(optionalPairResult.isPresent()){
            log.info("Find PairResult with id: " + optionalPairResult.get().getIdPairResult());

            return optionalPairResult.get();
        }

        log.warn("Not find PairResult with id: " + id);

        return new PairResult();
    }

    /**
     * ToDo
     * @return
     */
    @Override
    public List<PairResult> findAll() {
        List<PairResult> pairResults = pairResultRepository.findAll();

        if(!pairResults.isEmpty()){
            log.info("Find all PairResult");

            return pairResults;
        }

        log.warn("Not find all PairResult");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param poll
     * @return
     */
    public List<PairResult> findByPoll(Poll poll){
        if(Objects.nonNull(poll.getIdPoll())){
            List<PairResult> pairResults = pairResultRepository.findByPoll(poll);

            if(!pairResults.isEmpty()){
                log.info("Find PairResult by Poll with id: " + poll.getIdPoll());

                return  pairResults;
            }
        }

        log.warn("Not find PairResult by Poll with id: " + poll.getIdPoll());

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param teacher
     * @return
     */
    public List<PairResult> findByTeacher(Teacher teacher){
        if(Objects.nonNull(teacher.getIdTeacher())){
            List<PairResult> pairResults = pairResultRepository.findByTeacher(teacher);

            if(!pairResults.isEmpty()){
                log.info("Find PairResult by Teacher with id: " + teacher.getIdTeacher());

                return  pairResults;
            }
        }

        log.warn("Not find PairResult by Teacher with id: " + teacher.getIdTeacher());

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param student
     * @return
     */
    public List<PairResult> findByStudent(Student student){
        if(Objects.nonNull(student.getIdStudent())){
            List<PairResult> pairResults = pairResultRepository.findByStudent(student);

            if(!pairResults.isEmpty()){
                log.info("Find PairResult by Student with id: " + student.getIdStudent());

                return  pairResults;
            }
        }

        log.warn("Find PairResult by Student with id: " + student.getIdStudent());

        return new ArrayList<>();
    }

    /**
     *
     * @param student
     * @param poll
     * @return
     */
    public PairResult findByStudentAndPoll(Student student,Poll poll) {
        Optional<PairResult> optionalPairResult = pairResultRepository.findByStudentAndPoll(student,poll);

        if(optionalPairResult.isPresent()){
            log.info("Find PairResult by Poll with id: " + poll.getIdPoll() + ", and Student with id: " + student.getIdStudent());

            return optionalPairResult.get();
        }

        log.warn("Not find PairResult by Poll with id: " + poll.getIdPoll() + ", and Student with id: " + student.getIdStudent());

        return new PairResult();
    }

    /**
     *
     * @param teacher
     * @param poll
     * @return
     */
    public PairResult findByTeacherAndPoll(Teacher teacher,Poll poll) {
        Optional<PairResult> optionalPairResult = pairResultRepository.findByTeacherAndPoll(teacher,poll);

        if(optionalPairResult.isPresent()){
            log.info("Find PairResult by Poll with id: " + poll.getIdPoll() + ", and Teacher with id: " + teacher.getIdTeacher());

            return optionalPairResult.get();
        }

        log.warn("Find PairResult by Poll with id: " + poll.getIdPoll() + ", and Teacher with id: " + teacher.getIdTeacher());

        return new PairResult();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(PairResult entity) {
        log.warn("Delete PairResult with id: " + entity.getIdPairResult());

        pairResultRepository.delete(entity);

        return;
    }

}
