package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.repository.PollRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PollService implements DataService<Poll> {

    @Autowired
    private PollRepository pollRepository;

    @Autowired
    private TeacherChoiseService teacherChoiseService;

    @Autowired
    private StudentChoiseService studentChoiseService;

    @Autowired
    private PairResultService pairResultService;

    /**
     * ToDo
     * @param entity
     * @return
     */
    @Override
    public Poll save(Poll entity) {
        if(Objects.nonNull(entity.getTeacherCreator().getIdTeacher()) && Objects.nonNull(entity.getGroup().getIdGroup())){
            Poll saveEntity = pollRepository.save(entity);

            log.info("Save Poll with id: " + saveEntity.getIdPoll());

            return saveEntity;
        }

        log.warn("Teacher or Group not found. Not save Poll with name: " + entity.getNamePoll());

        return new Poll();
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public Poll findById(Long id) {
        Optional<Poll> optionalPoll = pollRepository.findById(id);

        if(optionalPoll.isPresent()){
            log.info("Find Poll with id: " + optionalPoll.get().getIdPoll());

            return optionalPoll.get();
        }

        log.warn("Not find Poll with id: " + id);

        return new Poll();
    }

    /**
     * ToDo
     * @return
     */
    @Override
    public List<Poll> findAll() {
        List<Poll> polls = pollRepository.findAll();

        if(!polls.isEmpty()){
            log.info("Find all Polls");

            return polls;
        }

        log.warn("Not find all Polls");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param group
     * @return
     */
    public List<Poll> findByGroup(Group group){
        if(Objects.nonNull(group.getIdGroup())){
            List<Poll> polls = pollRepository.findByGroup(group);

            if(!polls.isEmpty()){
                log.info("Find Polls by Group with id: " + group.getIdGroup());

                return polls;
            }
        }

        log.warn("Not find Polls by Group with id: " + group.getIdGroup());

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param teacherCreator
     * @return
     */
    public List<Poll> findByTeacherCreator(Teacher teacherCreator){
        if(Objects.nonNull(teacherCreator.getIdTeacher())){
            List<Poll> polls = pollRepository.findByTeacherCreator(teacherCreator);

            if(!polls.isEmpty()){
                log.info("Find Polls by TeacherCreator with id: " + teacherCreator.getIdTeacher());

                return polls;
            }
        }

        log.warn("Not find Polls by TeacherCreator with id: " + teacherCreator.getIdTeacher());

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param namePoll
     * @return
     */
    public List<Poll> findByNamePoll(String namePoll){
        if(Objects.nonNull(namePoll)){
            List<Poll> polls = pollRepository.findByNamePoll(namePoll);

            if(!polls.isEmpty()){
                log.info("Find Polls by name: " + namePoll);

                return polls;
            }
        }

        log.warn("Not find Polls by name poll: " + namePoll);

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(Poll entity) {
        List<StudentChoise> studentChoises = studentChoiseService.findByPoll(entity);

        studentChoises.forEach((studentChoise -> studentChoiseService.delete(studentChoise)));

        List<TeacherChoise> teacherChoises = teacherChoiseService.findByPoll(entity);

        teacherChoises.forEach(teacherChoise -> teacherChoiseService.delete(teacherChoise));

        List<PairResult> pairResults = pairResultService.findByPoll(entity);

        pairResults.forEach(pairResult -> pairResultService.delete(pairResult));

        log.warn("Delete Poll with id: " + entity.getIdPoll());

        pollRepository.delete(entity);

        return;
    }

}
