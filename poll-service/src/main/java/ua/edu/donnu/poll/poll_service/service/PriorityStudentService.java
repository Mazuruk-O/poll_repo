package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.PriorityStudent;
import ua.edu.donnu.poll.poll_service.domain.StudentChoise;
import ua.edu.donnu.poll.poll_service.repository.PriorityStudentRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PriorityStudentService implements DataService<PriorityStudent> {

    @Autowired
    private PriorityStudentRepository priorityStudentRepository;

    /**
     * ToDo
     * @param entity
     * @return
     */
    @Override
    public PriorityStudent save(PriorityStudent entity) {
        PriorityStudent saveEntity = priorityStudentRepository.save(entity);

        log.info("");

        return saveEntity;
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public PriorityStudent findById(Long id) {
        Optional<PriorityStudent> optionalPriorityStudent = priorityStudentRepository.findById(id);

        if(optionalPriorityStudent.isPresent()){
            log.info("");

            return optionalPriorityStudent.get();
        }

        log.warn("");

        return new PriorityStudent();
    }

    /**
     * ToDo
     * @return
     */
    @Override
    public List<PriorityStudent> findAll() {
        List<PriorityStudent> priorityStudents = priorityStudentRepository.findAll();

        if(!priorityStudents.isEmpty()){
            log.info("");

            return priorityStudents;
        }

        log.warn("");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param studentChoise
     * @return
     */
    public List<PriorityStudent> findByStudentChoise(StudentChoise studentChoise){
        if(Objects.nonNull(studentChoise.getIdStudentChoise())){
            List<PriorityStudent> priorityStudents = priorityStudentRepository.findByStudentChoise(studentChoise);

            if(!priorityStudents.isEmpty()){
                log.info("");

                return priorityStudents;
            }
        }

        log.warn("");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(PriorityStudent entity) {
        priorityStudentRepository.delete(entity);

        log.warn("");

        return;
    }
}
