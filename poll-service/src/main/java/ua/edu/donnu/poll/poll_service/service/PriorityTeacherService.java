package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.PriorityTeacher;
import ua.edu.donnu.poll.poll_service.domain.TeacherChoise;
import ua.edu.donnu.poll.poll_service.repository.PriorityTeacherRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PriorityTeacherService implements DataService<PriorityTeacher>{

    @Autowired
    private PriorityTeacherRepository priorityTeacherRepository;

    /**
     * ToDo
     * @param entity
     * @return
     */
    @Override
    public PriorityTeacher save(PriorityTeacher entity) {
        PriorityTeacher saveEntity = priorityTeacherRepository.save(entity);

        log.info("");

        return saveEntity;
    }

    /**
     * ToDO
     * @param id
     * @return
     */
    @Override
    public PriorityTeacher findById(Long id) {
        Optional<PriorityTeacher> optionalPriorityTeacher = priorityTeacherRepository.findById(id);

        if(optionalPriorityTeacher.isPresent()){
            log.info("");

            return optionalPriorityTeacher.get();
        }

        log.warn("");

        return new PriorityTeacher();
    }

    /**
     * ToDO
     * @return
     */
    @Override
    public List<PriorityTeacher> findAll() {
        List<PriorityTeacher> priorityTeachers = priorityTeacherRepository.findAll();

        if(!priorityTeachers.isEmpty()){
            log.info("");

            return priorityTeachers;
        }

        log.warn("");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param teacherChoise
     * @return
     */
    public List<PriorityTeacher> findByTeacherChoise(TeacherChoise teacherChoise){
        if(Objects.nonNull(teacherChoise.getIdTeacherChoise())){
            List<PriorityTeacher> priorityTeachers = priorityTeacherRepository.findByTeacherChoise(teacherChoise);

            if(!priorityTeachers.isEmpty()){
                log.info("");

                return priorityTeachers;
            }
        }

        log.warn("");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(PriorityTeacher entity) {
        priorityTeacherRepository.delete(entity);

        log.warn("");

        return;
    }
}
