package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.repository.StudentChoiseRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class StudentChoiseService implements DataService<StudentChoise> {

    @Autowired
    private StudentChoiseRepository studentChoiseRepository;

    /**
     * ToDo
     * @param entity
     * @return
     */
    @Override
    public StudentChoise save(StudentChoise entity) {
        studentChoiseRepository.save(entity);

        log.info("Save StudentChoise with id: " + entity.getIdStudentChoise());

        return entity;
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public StudentChoise findById(Long id) {
        Optional<StudentChoise> optionalStudentChoise = studentChoiseRepository.findById(id);

        if(optionalStudentChoise.isPresent()){
            log.info("Find StudentChoise with id: " + optionalStudentChoise.get().getIdStudentChoise());

            return optionalStudentChoise.get();
        }

        log.warn("Not find StudentChoise with id: " + id);

        return new StudentChoise();
    }

    @Override
    public List<StudentChoise> findAll() {
        List<StudentChoise> studentChoises = studentChoiseRepository.findAll();

        if(!studentChoises.isEmpty()){
            log.info("Find all StudentChoises");

            return studentChoises;
        }

        log.warn("Not find all StudentChoises");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param poll
     * @return
     */
    public List<StudentChoise> findByPoll(Poll poll){
        if(Objects.nonNull(poll.getIdPoll())){
            List<StudentChoise> studentChoises = studentChoiseRepository.findByPoll(poll);

            if(!studentChoises.isEmpty()){
                log.info("Find StudentChoises by Poll with id: " + poll.getIdPoll());

                return studentChoises;
            }
        }

        log.warn("Not find StudentChoises by Poll with id: " + poll.getIdPoll());

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param votingStudent
     * @return
     */
    public List<StudentChoise> findByVotingStudent(Student votingStudent){
        if(Objects.nonNull(votingStudent.getIdStudent())){
            List<StudentChoise> studentChoises = studentChoiseRepository.findByVotingStudent(votingStudent);

            if(!studentChoises.isEmpty()){
                log.info("Find StudentChoises by VotingStudent with id: " + votingStudent.getIdStudent());

                return studentChoises;
            }
        }

        log.warn("Find StudentChoises by VotingStudent with id: " + votingStudent.getIdStudent());

        return new ArrayList<>();
    }

    /**
     *
     * @param votingStudent
     * @param poll
     * @return
     */
    public StudentChoise findByVotingStudentAndPoll(Student votingStudent,Poll poll){
        Optional<StudentChoise> optionalStudentChoise = studentChoiseRepository.findByVotingStudentAndPoll(votingStudent,poll);

        if(optionalStudentChoise.isPresent()){
            log.info("Find StudentChoises by Poll with id: " + poll.getIdPoll() + ", and VotingStudent with id: " + votingStudent.getIdStudent());

            return optionalStudentChoise.get();
        }

        log.warn("Not Find StudentChoises by Poll with id: " + poll.getIdPoll() + ", and VotingStudent with id: " + votingStudent.getIdStudent());

        return new StudentChoise();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(StudentChoise entity) {
        log.warn("Delete StudentChoise with id: " + entity.getIdStudentChoise());

        studentChoiseRepository.delete(entity);

        return;
    }

}
