package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.client.AuthServiceClient;
import ua.edu.donnu.poll.poll_service.domain.Group;
import ua.edu.donnu.poll.poll_service.domain.Student;
import ua.edu.donnu.poll.poll_service.repository.StudentRepository;
import java.util.*;

@Slf4j
@Service
public class StudentService implements DataService<Student> {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private AuthServiceClient authClient;

    /**
     * ToDO: add save User in Auth-Service
     * @param entity - object to save
     * @return if the save is successful: saved object
     *                           or else: empty object
     */
    @Override
    public Student save(Student entity) {
        Student saveEntity = studentRepository.save(entity);

        log.info("Save student with id: " + saveEntity.getIdStudent());

        return saveEntity;
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public Student findById(Long id) {
        Optional<Student> optionalGroup = studentRepository.findById(id);

        if(optionalGroup.isPresent()){
            log.info("Find Student with id: " + optionalGroup.get().getIdStudent());

            return optionalGroup.get();
        }

        log.warn("Not find Student with id:" + id);

        return new Student();
    }

    /**
     *
     * @param username
     * @return
     */
    public Student findByUsername(String username) {
        Optional<Student> optionalGroup = studentRepository.findByUsername(username);

        if(optionalGroup.isPresent()){
            log.info("Find Student with username: " + optionalGroup.get().getUsername());

            return optionalGroup.get();
        }

        log.warn("Not find Student with username: " + username);

        return new Student();
    }

    /**
     * ToDO
     * @return
     */
    @Override
    public List<Student> findAll() {
        List<Student> students = studentRepository.findAll();

        if(!students.isEmpty()){
            log.info("Find all Students");

            return students;
        }

        log.warn("Not find all Student");

        return new ArrayList<>();
    }

    /**
     * ToDO
     * @param group
     * @return
     */
    public List<Student> findByGroup(Group group){
        if(Objects.nonNull(group.getIdGroup())){
            List<Student> students = studentRepository.findByGroup(group);

            if(!students.isEmpty()){
                log.info("Find Students by Group with id: " + group.getIdGroup());

                return students;
            }
        }

        log.warn("Not find Students by Group with id: " + group.getIdGroup());

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param firstname
     * @param lastname
     * @param surname
     * @return
     */
    public List<Student> findByFirstnameAndLastnameAndSurname(String firstname, String lastname, String surname){
        if(Objects.nonNull(firstname) && Objects.nonNull(lastname) && Objects.nonNull(surname)){
            List<Student> students = studentRepository.findByFirstnameStudentAndLastnameStudentAndSurnameStudent(firstname, lastname, surname);

            if(!students.isEmpty()){
                log.info("Find students by firstname, lastname, surname");

                return students;
            }
        }

        log.warn("Not find students by firtname, lastname, surname: " + firstname + " " + lastname + " " + surname);

        return new ArrayList<>();
    }

    /**
     * ToDO
     * @param entity
     */
    @Override
    public void delete(Student entity) {
        log.warn("Delete Student with id: " + entity.getIdStudent());

        studentRepository.delete(entity);

        return;
    }
}
