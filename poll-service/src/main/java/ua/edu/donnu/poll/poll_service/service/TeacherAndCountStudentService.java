package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.Poll;
import ua.edu.donnu.poll.poll_service.domain.Teacher;
import ua.edu.donnu.poll.poll_service.domain.TeacherAndCountStudent;
import ua.edu.donnu.poll.poll_service.repository.TeacherAndCountStudentRepository;
import java.util.*;

@Slf4j
@Service
public class TeacherAndCountStudentService implements DataService<TeacherAndCountStudent> {

    @Autowired
    private TeacherAndCountStudentRepository teacherAndCountStudentRepository;

    /**
     * ToDo
     * @param entity
     * @return
     */
    @Override
    public TeacherAndCountStudent save(TeacherAndCountStudent entity) {
        teacherAndCountStudentRepository.save(entity);

        log.info("Save TeacherAndCountStudent with id: " + entity.getIdTeacherAndCountStudent());

        return entity;
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public TeacherAndCountStudent findById(Long id) {
        Optional<TeacherAndCountStudent> optional = teacherAndCountStudentRepository.findById(id);

        if(optional.isPresent()){
            log.info("Find TeachersAndCountStudents by id: " + id);

            return optional.get();
        }

        log.warn("Not find TeachersAndCountStudents by id: " + id);

        return new TeacherAndCountStudent();
    }

    /**
     * ToDo
     * @return
     */
    @Override
    public List<TeacherAndCountStudent> findAll() {
        List<TeacherAndCountStudent> tacs = teacherAndCountStudentRepository.findAll();

        if(!tacs.isEmpty()){
            log.info("Find all TeachersAndCountStudents");

            return tacs;
        }

        log.warn("Not find all TeachersAndCountStudents");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param poll
     * @return
     */
    public List<TeacherAndCountStudent> findByPoll(Poll poll){
        if(Objects.nonNull(poll.getIdPoll())){
            List<TeacherAndCountStudent> tacs = teacherAndCountStudentRepository.findByPoll(poll);

            if(!tacs.isEmpty()){
                log.info("Find TeachersAndCountStudents by Poll with id: " + poll.getIdPoll());

                return  tacs;
            }
        }

        log.warn("Find TeachersAndCountStudents by Poll with id: " + poll.getIdPoll());

        return new ArrayList<>();
    }

    public List<TeacherAndCountStudent> findByTeacher(Teacher teacher){
        List<TeacherAndCountStudent> tacs = teacherAndCountStudentRepository.findByTeacher(teacher);

        if(!tacs.isEmpty()){
            log.info("Find TeachersAndCountStudents by Teacher with id: " + teacher.getIdTeacher());

            return tacs;
        }

        log.warn("Find TeachersAndCountStudents by Teacher with id: " + teacher.getIdTeacher());

        return new ArrayList<>();
    }

    public TeacherAndCountStudent findByTeacherAndPoll(Teacher teacher, Poll poll){
        Optional<TeacherAndCountStudent> optional = teacherAndCountStudentRepository.findByTeacherAndPoll(teacher,poll);

        if(optional.isPresent()){
            log.info("Find TeacherAndCountStudent by Teacher with id: " + teacher.getIdTeacher() + ", and Poll with id: " + poll.getIdPoll());

            return optional.get();
        }

        log.warn("Not find TeacherAndCountStudent by Teacher with id: " + teacher.getIdTeacher() + ", and Poll with id: " + poll.getIdPoll());

        return new TeacherAndCountStudent();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(TeacherAndCountStudent entity) {
        log.warn("Delete TeacherAndCountStudent with id: " + entity.getIdTeacherAndCountStudent());

        teacherAndCountStudentRepository.delete(entity);

        return;
    }
}
