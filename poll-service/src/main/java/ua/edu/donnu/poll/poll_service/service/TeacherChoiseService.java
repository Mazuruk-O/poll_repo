package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.repository.TeacherChoiseRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class TeacherChoiseService implements DataService<TeacherChoise> {

    @Autowired
    private TeacherChoiseRepository teacherChoiseRepository;

    /**
     * ToDo
     * @param entity
     * @return
     */
    @Override
    public TeacherChoise save(TeacherChoise entity) {
        teacherChoiseRepository.save(entity);

        log.info("Save TeacherChoise with id: " + entity.getIdTeacherChoise());

        return entity;
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public TeacherChoise findById(Long id) {
        Optional<TeacherChoise> optionalTeacherChoise = teacherChoiseRepository.findById(id);

        if (optionalTeacherChoise.isPresent()) {
            log.info("Find TeacherChoise with id: " + optionalTeacherChoise.get().getIdTeacherChoise());

            return optionalTeacherChoise.get();
        }

        log.warn("Not find TeacherChoise with id: " + id);

        return new TeacherChoise();
    }

    /**
     * ToDo
     * @return
     */
    @Override
    public List<TeacherChoise> findAll() {
        List<TeacherChoise> teacherChoises = teacherChoiseRepository.findAll();

        if (!teacherChoises.isEmpty()) {
            log.info("Find all TeacherChoises");

            return teacherChoises;
        }

        log.warn("Not find all TeacherChoises");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param poll
     * @return
     */
    public List<TeacherChoise> findByPoll(Poll poll) {
        if(Objects.nonNull(poll.getIdPoll())){
            List<TeacherChoise> teacherChoises = teacherChoiseRepository.findByPoll(poll);

            if(!teacherChoises.isEmpty()){
                log.info("Find TeacherChoises by Poll with id: " + poll.getIdPoll());

                return teacherChoises;
            }
        }

        log.warn("Not find TeacherChoises by Poll with id: " + poll.getIdPoll());

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param votingTeacher
     * @return
     */
    public List<TeacherChoise> findByVotingTeacher(Teacher votingTeacher){
        if(Objects.nonNull(votingTeacher.getIdTeacher())){
            List<TeacherChoise> teacherChoises = teacherChoiseRepository.findByVotingTeacher(votingTeacher);

            if(!teacherChoises.isEmpty()){
                log.info("Find TeacherChoises by VotingTeacher with id: " + votingTeacher.getIdTeacher());

                return teacherChoises;
            }
        }

        log.warn("Not find TeacherChoises by VotingTeacher with id: " + votingTeacher.getIdTeacher());

        return new ArrayList<>();
    }

    public TeacherChoise findByPollAndVotingTeacher(Poll poll, Teacher votingTeacher){
        Optional<TeacherChoise> optionalTeacherChoise = teacherChoiseRepository.findByPollAndVotingTeacher(poll,votingTeacher);

        if (optionalTeacherChoise.isPresent()) {
            log.info("Find StudentChoises by Poll with id: " + poll.getIdPoll() + ", and VotingStudent with id: " + votingTeacher.getIdTeacher());

            return optionalTeacherChoise.get();
        }

        log.warn("Not find teacher choise with poll id:" + poll.getIdPoll() + "and VotingTeacher id: " + votingTeacher.getIdTeacher());

        return new TeacherChoise();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(TeacherChoise entity) {
        log.warn("Delete TeacherChoise with id: " + entity.getIdTeacherChoise());

        teacherChoiseRepository.delete(entity);

        return;
    }

}
