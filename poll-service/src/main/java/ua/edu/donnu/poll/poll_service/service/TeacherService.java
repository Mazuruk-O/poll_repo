package ua.edu.donnu.poll.poll_service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.donnu.poll.poll_service.client.AuthServiceClient;
import ua.edu.donnu.poll.poll_service.domain.Teacher;
import ua.edu.donnu.poll.poll_service.repository.TeacherRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class TeacherService implements DataService<Teacher> {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private AuthServiceClient authClient;

    /**
     * ToDo: save user in Auth-service
     * @param entity
     * @return
     */
    @Override
    public Teacher save(Teacher entity) {
        Teacher saveEntity = teacherRepository.save(entity);

        log.info("Save Teacher with id: " + saveEntity.getIdTeacher());

        return saveEntity;
    }

    /**
     * ToDo
     * @param id
     * @return
     */
    @Override
    public Teacher findById(Long id) {
        Optional<Teacher> optionalTeacher = teacherRepository.findById(id);

        if(optionalTeacher.isPresent()){
            log.info("Find Teacher with id: " + optionalTeacher.get().getIdTeacher());

            return optionalTeacher.get();
        }

        log.warn("Not find teacher with id: " + id);

        return new Teacher();
    }

    /**
     *
     * @param username
     * @return
     */
    public Teacher findByUsername(String username) {
        Optional<Teacher> optionalGroup = teacherRepository.findByUsername(username);

        if(optionalGroup.isPresent()){
            log.info("Find Teacher with username: " + optionalGroup.get().getUsername());

            return optionalGroup.get();
        }

        log.warn("Not find Teacher with username: " + username);

        return new Teacher();
    }

    /**
     * ToDo
     * @return
     */
    @Override
    public List<Teacher> findAll() {
        List<Teacher> teachers = teacherRepository.findAll();

        if(!teachers.isEmpty()){
            log.info("Find all Teachers");

            return teachers;
        }

        log.warn("Not find all Teachers");

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param firstname
     * @param lastname
     * @param surname
     * @return
     */
    public List<Teacher> findByFirstnameAndLastnameAndSurname(String firstname, String lastname, String surname){
        if(Objects.nonNull(firstname) && Objects.nonNull(lastname) && Objects.nonNull(surname)){
            List<Teacher> teachers = teacherRepository.findByFirstnameTeacherAndLastnameTeacherAndSurnameTeacher(firstname, lastname, surname);

            if(!teachers.isEmpty()){
                log.info("Find teachers by firtname, lastname, surname");

                return teachers;
            }
        }

        log.warn("Not find teachers by firtname, lastname, surname: " + firstname + " " + lastname + " " + surname);

        return new ArrayList<>();
    }

    /**
     * ToDo
     * @param entity
     */
    @Override
    public void delete(Teacher entity) {
        log.warn("Delete Teacher with id: " + entity.getIdTeacher());

        teacherRepository.delete(entity);

        return;
    }
}
