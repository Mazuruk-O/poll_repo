package ua.edu.donnu.poll.poll_service.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import ua.edu.donnu.poll.poll_service.domain.security.User;

import java.util.Arrays;
import java.util.List;

//@TestConfiguration
public class SpringSecurityWebAuxTestConfig {
    //@Bean
    //@Primary
    public UserDetailsService userDetailsService() {
        User student = new User();
        student.setUsername("student");
        student.setPassword("password");
        student.setAccountNonExpired(true);
        student.setAccountNonLocked(true);
        student.setCredentialsNonExpired(true);
        student.setEnabled(true);
        List<GrantedAuthority> ga = Arrays.asList( new SimpleGrantedAuthority("ROLE_STUDENT"),
                                                    new SimpleGrantedAuthority("PERM_FOO_READ"));
        student.setGrantedAuthorities(ga);

        return new InMemoryUserDetailsManager(Arrays.asList(
               student
        ));
    }
}
