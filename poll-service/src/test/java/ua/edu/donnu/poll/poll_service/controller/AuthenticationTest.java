package ua.edu.donnu.poll.poll_service.controller;

import com.sun.security.auth.UserPrincipal;
import lombok.Data;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import ua.edu.donnu.poll.poll_service.domain.security.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Data
public class AuthenticationTest implements Authentication {

    private String name;

    private String password;

    private boolean isAuthenticated;

    private List<GrantedAuthority> grantedAuthorities;

    public AuthenticationTest(String name, String password, boolean isAuthenticated, List<GrantedAuthority> grantedAuthorities) {
        this.name = name;
        this.password = password;
        this.isAuthenticated = isAuthenticated;
        this.grantedAuthorities = grantedAuthorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public Object getCredentials() {
        return grantedAuthorities;
    }

    @Override
    public Object getDetails() {
        return this;
    }

    @Override
    public Object getPrincipal() {
        return new UserPrincipal(name);
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        this.isAuthenticated = b;
    }

    @Override
    public String getName() {
        return name;
    }
}
