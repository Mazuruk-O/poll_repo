package ua.edu.donnu.poll.poll_service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.discovery.converters.Auto;
import com.sun.security.auth.UserPrincipal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.edu.donnu.poll.poll_service.config.SpringSecurityWebAuxTestConfig;
import ua.edu.donnu.poll.poll_service.domain.*;
import ua.edu.donnu.poll.poll_service.domain.DTO.input.InputDtoStudentChoise;
import ua.edu.donnu.poll.poll_service.domain.DTO.output.OutputDtoAllPolls;
import ua.edu.donnu.poll.poll_service.service.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class StudentControllerTest {

    @InjectMocks
    protected StudentController studentController;

    @LocalServerPort
    private int port;

    private static final ObjectMapper mapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setup() {
        initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(studentController).build();
    }

    @Test
    public void testAllPoll() throws Exception {
        Faculty f = createFaculty();
        Group g = createGroup(f);
        Student s = createStudent(g, "user");
        Teacher t = createTeacher();
        Poll p = createPoll(g,t);
        String result = mapper.writeValueAsString(new OutputDtoAllPolls(Arrays.asList(p)));

        Authentication aut = new AuthenticationTest(s.getUsername(), "password",
                    true, Arrays.asList(new SimpleGrantedAuthority("STUDENT")));

        when(studentService.findByUsername(s.getUsername())).thenReturn(s);
        when(pollService.findByGroup(g)).thenReturn(Arrays.asList(p));

        mockMvc.perform(MockMvcRequestBuilders.get("/student/all-poll").principal(aut))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(result)));
    }

    @Test
    public void testIdPoll() throws Exception {
        Faculty f = createFaculty();
        Group g = createGroup(f);
        Student s = createStudent(g, "user2");
        Teacher t = createTeacher();
        Poll p = createPoll(g,t);
        StudentChoise stch = createStudentChoise(p,s);

        Authentication aut = new AuthenticationTest(s.getUsername(), "password",
                true, Arrays.asList(new SimpleGrantedAuthority("STUDENT")));

        when(studentService.findByUsername(s.getUsername())).thenReturn(s);
        when(pollService.findById(1L)).thenReturn(p);
        when(studentChoiseService.findById(1L)).thenReturn(new StudentChoise());
        when(teacherAndCountStudentService.findByPoll(p)).thenReturn(Arrays.asList(new TeacherAndCountStudent()));
        when(priorityStudentService.findByStudentChoise(stch)).thenReturn(Arrays.asList(new PriorityStudent()));
        when(pairResultService.findByStudentAndPoll(s,p)).thenReturn(new PairResult());

        mockMvc.perform(MockMvcRequestBuilders.get("/student/poll/"+1L).principal(aut))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreatePoll() throws Exception {
        Faculty f = createFaculty();
        Group g = createGroup(f);
        Student s = createStudent(g, "user");
        Teacher t = createTeacher();
        Poll p = createPoll(g,t);
        InputDtoStudentChoise dto = new InputDtoStudentChoise();
        dto.setPoll(p);
        dto.setPriorityStudent(Arrays.asList(createPriorityStudent(createStudentChoise(p,s) ,t)));
        String inputDto = mapper.writeValueAsString(dto);

        Authentication aut = new AuthenticationTest(s.getUsername(), "password",
                true, Arrays.asList(new SimpleGrantedAuthority("STUDENT")));

        when(studentService.findByUsername(s.getUsername())).thenReturn(s);
        when(pollService.findByGroup(g)).thenReturn(Arrays.asList(p));
        when(pollService.findById(null)).thenReturn(p);

        mockMvc.perform(MockMvcRequestBuilders.post("/student/poll").principal(aut)
                .contentType(MediaType.APPLICATION_JSON).content(inputDto))
                .andExpect(status().isOk());
    }

    private PriorityStudent createPriorityStudent(StudentChoise stch, Teacher t) {
        PriorityStudent ps = new PriorityStudent();
        ps.setTeacher(t);
        ps.setPriority(1L);
        ps.setStudentChoise(stch);
        return ps;
    }

    private StudentChoise createStudentChoise(Poll p, Student s) {
        StudentChoise stch = new StudentChoise();
        stch.setPoll(p);
        stch.setVotingStudent(s);
        studentChoiseService.save(stch);
        return stch;
    }

    private TeacherAndCountStudent createTeacherAndCountStudent(Teacher t, Poll p) {
        TeacherAndCountStudent tacs = new TeacherAndCountStudent();
        tacs.setTeacher(t);
        tacs.setPoll(p);
        tacs.setCountStudent(5L);
        teacherAndCountStudentService.save(tacs);
        return tacs;
    }

    private Poll createPoll(Group group, Teacher teacher){
        Poll p = new Poll();
        p.setNamePoll("name_" + idObj++);
        p.setTeacherCreator(teacher);
        p.setGroup(group);
        p.setDateEndPoll(new Date(System.currentTimeMillis()));
        pollService.save(p);
        return p;
    }

    private Teacher createTeacher(){
        Teacher t = new Teacher();
        t.setFirstnameTeacher("firstname_" + idObj++);
        t.setLastnameTeacher("lastname_" + idObj++);
        t.setSurnameTeacher("surname_" + idObj++);
        t.setUsername("username_" + idObj++);
        teacherService.save(t);
        return t;
    }

    private Student createStudent(Group group, String username){
        Student s = new Student();
        s.setFirstnameStudent("firstname_" + idObj++);
        s.setLastnameStudent("lastname_" + idObj++);
        s.setSurnameStudent("surname_" + idObj++);
        s.setUsername(username);
        s.setGroup(group);
        studentService.save(s);
        return s;
    }

    private Group createGroup(Faculty faculty){
        Group g = new Group();
        g.setFaculty(faculty);
        g.setFullNameGroup("full_" + idObj++);
        g.setShortNameGroup("short_" + idObj++);
        groupService.save(g);
        return g;
    }

    private Faculty createFaculty(){
        Faculty f = new Faculty();
        f.setFullNameFaculty("full_" + idObj++);
        f.setShortNameFaculty("short_" + idObj++);
        facultyService.save(f);
        return f;
    }

    protected static int idObj = 0;

    @Mock
    private FacultyService facultyService;

    @Mock
    private GroupService groupService;

    @Mock
    private TeacherService teacherService;

    @Mock
    private PollService pollService;

    @Mock
    private StudentChoiseService studentChoiseService;

    @Mock
    private StudentService studentService;

    @Mock
    private PairResultService pairResultService;

    @Mock
    private PriorityStudentService priorityStudentService;

    @Mock
    private TeacherAndCountStudentService teacherAndCountStudentService;

}