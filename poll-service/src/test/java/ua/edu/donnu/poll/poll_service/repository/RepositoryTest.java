package ua.edu.donnu.poll.poll_service.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.edu.donnu.poll.poll_service.domain.*;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTest {

    @Autowired
    protected FacultyRepository facultyRepository;

    @Autowired
    protected GroupRepository groupRepository;

    @Autowired
    protected StudentRepository studentRepository;

    @Autowired
    protected TeacherRepository teacherRepository;

    @Autowired
    protected PollRepository pollRepository;

    @Autowired
    protected TeacherAndCountStudentRepository tacsr;

    @Autowired
    protected PairResultRepository pairResultRepository;

    @Test
    public void pairResultTest(){
        Faculty f = createFaculty();
        Group g = createGroup(f);
        Student s = createStudent(g);
        Teacher t = createTeacher();
        Poll p = createPoll(g,t);
        PairResult pr = createPairResult(p,t,s);
        assertNotNull(pr.getIdPairResult());

        List<PairResult> byPoll = pairResultRepository.findByPoll(p);
        List<PairResult> byStudent = pairResultRepository.findByStudent(s);
        List<PairResult> byTeacher = pairResultRepository.findByTeacher(t);
        PairResult byStudentPoll = pairResultRepository.findByStudentAndPoll(s,p).get();
        PairResult byTeacherPoll = pairResultRepository.findByTeacherAndPoll(t,p).get();

        assertEquals(pr.getIdPairResult(), byStudentPoll.getIdPairResult());
        assertEquals(pr.getIdPairResult(), byTeacherPoll.getIdPairResult());
        assertEquals(pr.getIdPairResult(), byPoll.get(0).getIdPairResult());
        assertEquals(pr.getIdPairResult(), byStudent.get(0).getIdPairResult());
        assertEquals(pr.getIdPairResult(), byTeacher.get(0).getIdPairResult());

        pairResultRepository.delete(pr);
        assertTrue(pairResultRepository.findAll().isEmpty());
        pollRepository.delete(p);
        teacherRepository.delete(t);
        studentRepository.delete(s);
        groupRepository.delete(g);
        facultyRepository.delete(f);
    }

    private PairResult createPairResult(Poll p, Teacher t, Student s) {
        PairResult pr = new PairResult();
        pr.setStudent(s);
        pr.setTeacher(t);
        pr.setPoll(p);
        pairResultRepository.save(pr);
        return pr;
    }

    @Test
    public void teacherAndCountStudentTest(){
        Faculty f = createFaculty();
        Group g = createGroup(f);
        Teacher t = createTeacher();
        Poll p = createPoll(g,t);
        TeacherAndCountStudent tacs = createTeacherAndCountStudent(t,p);

        List<TeacherAndCountStudent> byTeacher = tacsr.findByTeacher(t);
        List<TeacherAndCountStudent> byPoll = tacsr.findByPoll(p);
        TeacherAndCountStudent byTP = tacsr.findByTeacherAndPoll(t,p).get();
        List<TeacherAndCountStudent> all = tacsr.findAll();

        assertEquals(tacs.getIdTeacherAndCountStudent(),byTeacher.get(0).getIdTeacherAndCountStudent());
        assertEquals(tacs.getIdTeacherAndCountStudent(),byPoll.get(0).getIdTeacherAndCountStudent());
        assertEquals(tacs.getIdTeacherAndCountStudent(),all.get(0).getIdTeacherAndCountStudent());
        assertEquals(tacs.getIdTeacherAndCountStudent(),byTP.getIdTeacherAndCountStudent());

        tacsr.delete(tacs);
        pollRepository.delete(p);
        teacherRepository.delete(t);
        groupRepository.delete(g);
        facultyRepository.delete(f);

        assertTrue(tacsr.findAll().isEmpty());
    }

    @Test
    public void pollRepositoryTest(){
        Faculty f = createFaculty();
        Group g = createGroup(f);
        Teacher t = createTeacher();

        Poll p = createPoll(g,t);

        Poll byGroup = pollRepository.findByGroup(g).get(0);
        Poll byTeacher = pollRepository.findByTeacherCreator(t).get(0);
        Poll byName = pollRepository.findByNamePoll("name_1").get(0);
        List<Poll> polls = pollRepository.findAll();

        assertNotNull(byGroup);
        assertNotNull(byTeacher);
        assertNotNull(byName);
        assertFalse(polls.isEmpty());

        assertEquals(p.getGroup(), byGroup.getGroup());
        assertEquals(p.getTeacherCreator(), byTeacher.getTeacherCreator());
        assertEquals(p.getNamePoll(), byName.getNamePoll());
        assertEquals(p.getNamePoll(), polls.get(0).getNamePoll());

        pollRepository.delete(p);
        List<Poll> afterDelete = pollRepository.findAll();
        assertTrue(afterDelete.isEmpty());

        teacherRepository.delete(t);
        groupRepository.delete(g);
        facultyRepository.delete(f);
    }

    @Test
    public void teacherRepositoryTest(){
        Teacher t = createTeacher();
        assertNotNull(t.getIdTeacher());

        Teacher byUsername = teacherRepository.findByUsername("username").get();
        Teacher byName = teacherRepository.findByFirstnameTeacherAndLastnameTeacherAndSurnameTeacher("firstname",
                                                                          "lastname",
                                                                          "surname").get(0);
        List<Teacher> teachers = teacherRepository.findAll();

        assertNotNull(byUsername);
        assertNotNull(byName);
        assertFalse(teachers.isEmpty());

        assertNotNull(byUsername.getIdTeacher());
        assertNotNull(byName.getIdTeacher());
        assertNotNull(teachers.get(0).getIdTeacher());

        assertEquals(t.getUsername(), byUsername.getUsername());
        assertEquals(t.getFirstnameTeacher()+t.getLastnameTeacher()+t.getSurnameTeacher(),
                byName.getFirstnameTeacher()+byName.getLastnameTeacher()+byName.getSurnameTeacher());

        teacherRepository.delete(t);
        List<Teacher> afterDelete = teacherRepository.findAll();
        assertTrue(afterDelete.isEmpty());
    }

    @Test
    public void studentRepositoryTest(){
        Faculty f = createFaculty();

        Group g = createGroup(f);

        Student s = createStudent(g);
        assertNotNull(s.getIdStudent());

        Student byGroup = studentRepository.findByGroup(g).get(0);
        Student byUsername = studentRepository.findByUsername("username").get();
        Student byName = studentRepository.findByFirstnameStudentAndLastnameStudentAndSurnameStudent("firstname",
                                                                                                     "lastname",
                                                                                                     "surname").get(0);
        List<Student> students = studentRepository.findAll();

        assertNotNull(byGroup);
        assertNotNull(byUsername);
        assertNotNull(byName);
        assertFalse(students.isEmpty());

        assertNotNull(byGroup.getIdStudent());
        assertNotNull(byUsername.getIdStudent());
        assertNotNull(byName.getIdStudent());

        assertEquals(s.getGroup(), byGroup.getGroup());
        assertEquals(s.getUsername(), byUsername.getUsername());
        assertEquals(s.getFirstnameStudent()+s.getLastnameStudent()+s.getSurnameStudent(),
                byName.getFirstnameStudent()+byName.getLastnameStudent()+byName.getSurnameStudent());

        studentRepository.delete(s);
        List<Student> afterDelete = studentRepository.findAll();
        assertTrue(afterDelete.isEmpty());

        groupRepository.delete(g);
        facultyRepository.delete(f);
    }

    @Test
    public void groupRepositoryTest(){
        Faculty f = createFaculty();

        Group g = createGroup(f);

        Group fullName = groupRepository.findByFullNameGroup("full");
        List<Group> byFaculty = groupRepository.findGroupsByFaculty(f);

        assertEquals(g.getFullNameGroup(),fullName.getFullNameGroup());
        assertEquals(g.getShortNameGroup(),fullName.getShortNameGroup());
        assertNotNull(fullName.getIdGroup());

        assertNotNull(byFaculty);
        assertEquals(1,byFaculty.size());

        groupRepository.delete(g);
        List<Group> afterDelete = groupRepository.findAll();
        assertTrue(afterDelete.isEmpty());

        facultyRepository.delete(f);
    }

    @Test
    public void facultyRepositoryTest() {
        Faculty f = createFaculty();

        Faculty find = facultyRepository.findByFullNameFaculty("1");

        System.out.println(find);

        assertEquals(f.getFullNameFaculty(), find.getFullNameFaculty());
        assertEquals(f.getShortNameFaculty(), find.getShortNameFaculty());
        assertNotNull(find.getIdFaculty());

        List<Faculty> faculties = facultyRepository.findAll();
        assertNotNull(faculties);
        assertEquals(1,faculties.size());

        facultyRepository.delete(f);
        faculties = facultyRepository.findAll();
        assertTrue(faculties.isEmpty());
    }

    private TeacherAndCountStudent createTeacherAndCountStudent(Teacher t, Poll p) {
        TeacherAndCountStudent tacs = new TeacherAndCountStudent();
        tacs.setTeacher(t);
        tacs.setPoll(p);
        tacs.setCountStudent(5L);
        tacsr.save(tacs);
        return tacs;
    }

    private Poll createPoll(Group group, Teacher teacher){
        Poll p = new Poll();
        p.setNamePoll("name_1");
        p.setTeacherCreator(teacher);
        p.setGroup(group);
        p.setDateEndPoll(new Date(System.currentTimeMillis()));
        pollRepository.save(p);
        return p;
    }

    private Teacher createTeacher(){
        Teacher t = new Teacher();
        t.setFirstnameTeacher("firstname");
        t.setLastnameTeacher("lastname");
        t.setSurnameTeacher("surname");
        t.setUsername("username");
        teacherRepository.save(t);
        return t;
    }

    private Student createStudent(Group group){
        Student s = new Student();
        s.setFirstnameStudent("firstname");
        s.setLastnameStudent("lastname");
        s.setSurnameStudent("surname");
        s.setUsername("username");
        s.setGroup(group);
        studentRepository.save(s);
        return s;
    }

    private Group createGroup(Faculty faculty){
        Group g = new Group();
        g.setFaculty(faculty);
        g.setFullNameGroup("full");
        g.setShortNameGroup("short");
        groupRepository.save(g);
        return g;
    }

    private Faculty createFaculty(){
        Faculty f = new Faculty();
        f.setFullNameFaculty("1");
        f.setShortNameFaculty("2");
        facultyRepository.save(f);
        return f;
    }
}