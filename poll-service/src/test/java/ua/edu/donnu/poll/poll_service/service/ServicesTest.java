package ua.edu.donnu.poll.poll_service.service;

import org.hibernate.ObjectDeletedException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.edu.donnu.poll.poll_service.domain.*;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServicesTest {

    @Autowired
    protected FacultyService facultyService;

    @Autowired
    protected GroupService groupService;

    @Autowired
    protected StudentService studentService;

    @Autowired
    protected TeacherService teacherService;

    @Autowired
    protected PollService pollService;

    @Autowired
    protected TeacherAndCountStudentService tacss;

    @Autowired
    protected PairResultService pairResultService;

    protected static int idObj = 0;

    @Test
    public void pollRepositoryTest(){
        Faculty f = createFaculty();
        Group g = createGroup(f);
        Teacher t = createTeacher();

        Poll p = createPoll(g,t);

        Poll byGroup = pollService.findByGroup(g).get(0);
        Poll byTeacher = pollService.findByTeacherCreator(t).get(0);
        Poll byName = pollService.findByNamePoll(p.getNamePoll()).get(0);
        List<Poll> polls = pollService.findAll();

        assertNotNull(byGroup);
        assertNotNull(byTeacher);
        assertNotNull(byName);
        assertFalse(polls.isEmpty());

        assertEquals(p.getGroup(), byGroup.getGroup());
        assertEquals(p.getTeacherCreator(), byTeacher.getTeacherCreator());
        assertEquals(p.getNamePoll(), byName.getNamePoll());
        assertEquals(p.getNamePoll(), polls.get(0).getNamePoll());

        pollService.delete(p);
        List<Poll> afterDelete = pollService.findAll();
        assertTrue(afterDelete.isEmpty());
    }

    @Test
    public void teacherRepositoryTest(){
        Teacher t = createTeacher();
        assertNotNull(t.getIdTeacher());

        Teacher byUsername = teacherService.findByUsername(t.getUsername());
        Teacher byName = teacherService.findByFirstnameAndLastnameAndSurname(t.getFirstnameTeacher(),
                t.getLastnameTeacher(),
                t.getSurnameTeacher()).get(0);
        List<Teacher> teachers = teacherService.findAll();

        assertNotNull(byUsername);
        assertNotNull(byName);
        assertFalse(teachers.isEmpty());

        assertNotNull(byUsername.getIdTeacher());
        assertNotNull(byName.getIdTeacher());
        assertNotNull(teachers.get(0).getIdTeacher());

        assertEquals(t.getUsername(), byUsername.getUsername());
        assertEquals(t.getFirstnameTeacher()+t.getLastnameTeacher()+t.getSurnameTeacher(),
                byName.getFirstnameTeacher()+byName.getLastnameTeacher()+byName.getSurnameTeacher());

        teacherService.delete(t);
        List<Teacher> afterDelete = teacherService.findAll();
        assertTrue(afterDelete.isEmpty());
    }

    @Test
    public void studentRepositoryTest(){
        Faculty f = createFaculty();

        Group g = createGroup(f);

        Student s = createStudent(g);
        assertNotNull(s.getIdStudent());

        Student byGroup = studentService.findByGroup(g).get(0);
        Student byUsername = studentService.findByUsername(s.getUsername());
        Student byName = studentService.findByFirstnameAndLastnameAndSurname(s.getFirstnameStudent(),
                s.getLastnameStudent(),
                s.getSurnameStudent()).get(0);
        List<Student> students = studentService.findAll();

        assertNotNull(byGroup);
        assertNotNull(byUsername);
        assertNotNull(byName);
        assertFalse(students.isEmpty());

        assertNotNull(byGroup.getIdStudent());
        assertNotNull(byUsername.getIdStudent());
        assertNotNull(byName.getIdStudent());

        assertEquals(s.getGroup(), byGroup.getGroup());
        assertEquals(s.getUsername(), byUsername.getUsername());
        assertEquals(s.getFirstnameStudent()+s.getLastnameStudent()+s.getSurnameStudent(),
                byName.getFirstnameStudent()+byName.getLastnameStudent()+byName.getSurnameStudent());

        studentService.delete(s);
        List<Student> afterDelete = studentService.findAll();
        assertTrue(afterDelete.isEmpty());
    }

    @Test
    public void groupRepositoryTest(){
        Faculty f = createFaculty();

        Group g = createGroup(f);

        Group fullName = groupService.findByFullNameGroup(g.getFullNameGroup());
        List<Group> byFaculty = groupService.findGroupsByFaculty(f);

        assertEquals(g.getFullNameGroup(),fullName.getFullNameGroup());
        assertEquals(g.getShortNameGroup(),fullName.getShortNameGroup());
        assertNotNull(fullName.getIdGroup());

        assertNotNull(byFaculty);
        assertEquals(1,byFaculty.size());
    }

    @Test(expected = ObjectDeletedException.class)
    public void deleteGroup(){
        groupService.delete(createGroup(createFaculty()));
    }

    @Test
    public void facultyRepositoryTest() {
        Faculty f = createFaculty();

        Faculty find = facultyService.findByFullNameFaculty(f.getFullNameFaculty());

        System.out.println(find);

        assertEquals(f.getFullNameFaculty(), find.getFullNameFaculty());
        assertEquals(f.getShortNameFaculty(), find.getShortNameFaculty());
        assertNotNull(find.getIdFaculty());

        List<Faculty> faculties = facultyService.findAll();
        assertNotNull(faculties);
        assertFalse(faculties.isEmpty());
    }

    @Test(expected = ObjectDeletedException.class)
    public void deleteFaculty(){
        facultyService.delete(createFaculty());
    }

    private TeacherAndCountStudent createTeacherAndCountStudent(Teacher t, Poll p) {
        TeacherAndCountStudent tacs = new TeacherAndCountStudent();
        tacs.setTeacher(t);
        tacs.setPoll(p);
        tacs.setCountStudent(5L);
        tacss.save(tacs);
        return tacs;
    }

    private Poll createPoll(Group group, Teacher teacher){
        Poll p = new Poll();
        p.setNamePoll("name_" + idObj++);
        p.setTeacherCreator(teacher);
        p.setGroup(group);
        p.setDateEndPoll(new Date(System.currentTimeMillis()));
        pollService.save(p);
        return p;
    }

    private Teacher createTeacher(){
        Teacher t = new Teacher();
        t.setFirstnameTeacher("firstname_" + idObj++);
        t.setLastnameTeacher("lastname_" + idObj++);
        t.setSurnameTeacher("surname_" + idObj++);
        t.setUsername("username_" + idObj++);
        teacherService.save(t);
        return t;
    }

    private Student createStudent(Group group){
        Student s = new Student();
        s.setFirstnameStudent("firstname_" + idObj++);
        s.setLastnameStudent("lastname_" + idObj++);
        s.setSurnameStudent("surname_" + idObj++);
        s.setUsername("username_" + idObj++);
        s.setGroup(group);
        studentService.save(s);
        return s;
    }

    private Group createGroup(Faculty faculty){
        Group g = new Group();
        g.setFaculty(faculty);
        g.setFullNameGroup("full_" + idObj++);
        g.setShortNameGroup("short_" + idObj++);
        groupService.save(g);
        return g;
    }

    private Faculty createFaculty(){
        Faculty f = new Faculty();
        f.setFullNameFaculty("full_" + idObj++);
        f.setShortNameFaculty("short_" + idObj++);
        facultyService.save(f);
        return f;
    }

}
